#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include "socket_state.h"
#include "linker.h"
#include <vector>
#include <map>
#include <sstream>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <pthread.h>
#include <queue>

class tcp_server : public boost::enable_shared_from_this<tcp_server>
{
public:
    typedef boost::asio::ip::tcp::acceptor acceptor;
    tcp_server(boost::asio::io_service & io_service);
    void send_ping();
    void process_client_messages();
    void clean_clients();
    void close();

private:
    // The list of clients.
    std::map<std::string, socket_state::pointer> clients;

    std::map<std::string, std::string> userCellMap;
    // The list of threads to manage.
    std::vector<pthread_t> task_threads;
    int nextClientID;

    void start_accept();
    void handle_first_contact(socket_state::pointer & sock_state,
			      const boost::system::error_code & error);

    void notify_other_clients(std::string & orig_id, std::string & msg, bool excluse_self);

    // The TCP listener from boost::asio
    acceptor acceptor_;
    // The mutex objects locking the queue and client list for proper multithreading purposes.
    pthread_mutex_t cli_mutex;
    pthread_mutex_t queue_mutex;

    bool server_is_up;

    // The queue holding all incoming client messages.
    std::queue<std::string> msgQueue;

    linker sheet_linker;
};

#endif
