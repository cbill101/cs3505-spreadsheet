#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <stdio.h>
#include <fstream>

class spreadsheet   
{
  private:
  std::string name;
  std::map<std::string, std::string> currentSpreadsheet;
  std::map<std::string, std::vector<std::string> > revertList;
  std::vector<std::pair<std::string, std::string> > undoList;


public:
  void editCell (std::string cell, std::string content);
  std::string revert(std::string cell);
  std::pair<std::string, std::string> undo();
  std::map<std::string, std::string> getSpreadsheet();
  void saveFile(spreadsheet saveSpreadsheet);

  spreadsheet ();
  spreadsheet(std::string inName);
};
