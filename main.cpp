#include "tcp_server.h"

void* runServer(void* io)
{
    boost::asio::io_service *serv_ptr = (boost::asio::io_service *)io;
    // The io_service object provides I/O services, such as sockets, 
    // that the server object will use.

    std::cout << "Listening for spreadsheet clients..." << std::endl;

    serv_ptr->run();
}

int main()
{
  try
  {
    std::vector<pthread_t>threadList;

    boost::asio::io_service io_service;

    boost::asio::io_service * serv_ptr = &io_service;

    tcp_server server(*serv_ptr);

    pthread_t run_server;
    int rc = pthread_create(&run_server, NULL, runServer, (void *)serv_ptr);
    threadList.push_back(run_server);

    while(1)
    {
	std::string str;
	getline(std::cin, str);

	if(str == "q" || str == "quit")
	{
	    // Send disconnect messages to all clients...
	    serv_ptr->stop();
	    server.close();
	    pthread_join(threadList[0], NULL);
	    std::cout << "Stopped the server successfully." << std::endl;
	    return 0;
	}
	else
	{
	    std::cout << "\n"
		      << "Commands: \n"
		      << "quit (q): Shuts down the server and all connections.\n"
		      << std::endl;
	}
    } 
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}
