#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include "spreadsheet.h"

using namespace std;

spreadsheet::spreadsheet () : revertList(), undoList(), currentSpreadsheet()
{
   name = "";
}

spreadsheet::spreadsheet(string inName) : revertList(), undoList(), currentSpreadsheet()
{
    std::stringstream filename;
    filename << inName << ".txt";
    name = inName;
    ifstream ifile;

    std::string file_name = filename.str();

    ifile.open(file_name.c_str());
    if (ifile) 
    {
	string x;
	while(getline(ifile,x))
	{
            string buf; 
            stringstream ss(x); 

            vector<string> tokens; 

            while (ss >> buf)
		tokens.push_back(buf);

            if(tokens.size() == 0)
            {
		
	    }
	    else if(!tokens[0].compare("C:"))
            {
		if(tokens.size() == 2)
		    currentSpreadsheet[tokens[1]] = "";
		else
		{
          
		    string add = "";
		    for(int i = 2; i < tokens.size();i++)
			add += tokens[i]+ " ";
           
		    add = add.substr(0, add.size() - 1);
		    currentSpreadsheet[tokens[1]] = add;

		}
            
            }
	    else if(!tokens[0].compare("R:"))
            {
		if(tokens.size() == 2)
		    revertList[tokens[1]].push_back("");

		else
		{                        
		    string add2 = "";
		    for(int i = 2; i < tokens.size(); i++)
			add2 += tokens[i]+ " ";

		    add2 = add2.substr(0, add2.size() - 1);
		    revertList[tokens[1]].push_back(add2);
		}
            }
	    else if(!tokens[0].compare("U:"))
            {
		if(tokens.size() == 2)
		    undoList.push_back(pair<string, string>(tokens[1], ""));

                else
                {
		    string add3  ="";   
		    for(int i = 2; i < tokens.size(); i++)
			add3 += tokens[i]+ " "; 
                               
		    add3 = add3.substr(0, add3.size() - 1);        
		    undoList.push_back(pair<string, string>(tokens[1], add3));
		}
	    }

	}
    }
    ifile.close();
}

/*
*
*
*/
void spreadsheet::editCell (string cell, string content)
{
    revertList[cell].push_back(currentSpreadsheet[cell]);
    undoList.push_back(pair< string, string>(cell, currentSpreadsheet[cell]));
    currentSpreadsheet[cell] = content;
}

string spreadsheet::revert(string cell)
{
    if(revertList.find (cell) == revertList.end())
    {
	// No entry in revert list, rip, causes segfault!!!!
	return "";
    }

    string current = revertList[cell].back();

    if(revertList[cell].size() > 1)
	revertList[cell].pop_back();

    undoList.push_back(pair< string, string>(cell, current));
    currentSpreadsheet[cell] = current;
    return current;
}

pair< string, string> spreadsheet::undo()
{
    if(undoList.empty())
    {
	pair<string, string> ret("", "");
	return ret;
    }

    pair< string, string> toreturn = undoList.back();

    if(undoList.size() > 1)
	undoList.pop_back();

    currentSpreadsheet[toreturn.first] = toreturn.second;
    return toreturn;
}

map<string, string>  spreadsheet::getSpreadsheet()
{
    return currentSpreadsheet;
}

void spreadsheet::saveFile(spreadsheet saveSpreadsheet)
{
    string fileName = name + ".txt";
    ofstream file;
  
    file.open(fileName.c_str(), ios::out | ios::trunc);

    for (map<string, string>::iterator it = saveSpreadsheet.currentSpreadsheet.begin(); it != saveSpreadsheet.currentSpreadsheet.end(); it++)
    {
	file << "C: " << it->first << " " << it-> second << "\n";
    }

    for (map<string, vector<string> >::iterator it2 = saveSpreadsheet.revertList.begin(); it2 != saveSpreadsheet.revertList.end(); it2++)
    {
	for(int i = 0; i < it2 -> second.size(); i++)
	    file << "R: " << it2->first <<  " " << it2-> second[i] << "\n";
    }

    for(int i = 0; i < saveSpreadsheet.undoList.size(); i++)
	file << "U: " << saveSpreadsheet.undoList[i].first <<  " " << saveSpreadsheet.undoList[i].second << "\n";

    file.close(); 
}
