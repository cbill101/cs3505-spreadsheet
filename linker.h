#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include "spreadsheet.h"
#include <boost/filesystem.hpp>

class linker  
{
  private:
  std::map<std::string, spreadsheet>  spreadsheetList;
  std::vector<std::string> sheetNames;
  std::vector<std::string> getSheetNames();
 
public:
  typedef boost::filesystem::path path;
  std::string decode (std::string message, std::string client);

  linker ();
};
