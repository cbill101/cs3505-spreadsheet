#include "socket_state.h"
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <vector>

/*
 * Constructor:
 * Creates a new socket state object containing a socket, ID,
 * data buffer, and other information.
 * Args:
 * io_service: The asio service that accepts clients and manages async operations.
 */
socket_state::socket_state(boost::asio::io_service & io_service)
    : socket_(io_service), ETX(1, static_cast<char>(3)), data_buffer(), cliMsgQueue()
{
    ss_ID = "";
    ping_ticks = 0;
    assigned_sheet = "";
    closed = false;
    finished_handshake = false;
}

/*
 * Lets the socket state know about the queue to send messags to.
 * Also gives it a mutex to ensure message synchronization.
 */
void socket_state::assignServerInfo(std::queue<std::string> & serv_queue, pthread_mutex_t & queue_mutex_ptr)
{
    serverQueue = &serv_queue;
    server_queue_mutex = &queue_mutex_ptr;
}

/*
 * A wrapper method. Sends the passed in string data to the client.
 */
void socket_state::send(std::string & data)
{
    if(closed)
	return;

    socket_.send(boost::asio::buffer(data));
}

/*
 * Closes the socket within the socet state object.
 */
void socket_state::close()
{
    boost::system::error_code ec;
    boost::system::error_code sock_code;
    socket_.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
    socket_.cancel();
    socket_.close(sock_code);
    std::cout << sock_code.message() << std::endl;
    closed = true;
}

/*
 * Adds a ping tick. 1 ping tick = 10 seconds.
 * 6 ping ticks = disconnect.
 */
void socket_state::addPingTick()
{
    ping_ticks++;
}

/*
 * Retuns the current number of ping ticks.
 */
int socket_state::getPingTicks()
{
    return ping_ticks;
}

/*
 * Resets ping ticks upon receiving a ping_response.
 */
void socket_state::resetPingTicks()
{
    ping_ticks = 0;
}

/*
 * Async callback that receives incoming data and hands it to the server incoming queue.
 * The message sent to the queue is the socket's ID + the message received.
 */
void socket_state::handle_read(const boost::system::error_code& e, size_t t_size)
{
    if(!e)
    {
	if(t_size > 0)
	{
	    std::vector<std::string> split_data;

	    std::istream is(&data_buffer);
	    std::string cmd;
	    std::getline(is, cmd);

	    std::stringstream send_back;
	
	    boost::split(split_data, cmd, boost::is_any_of(ETX));

	    if(split_data.size() > 1)
	    {
		for(int idx = 0; idx < split_data.size() - 1; idx++)
		{
		    send_back << ss_ID << "\r" << split_data[idx] << ETX;
		    cliMsgQueue.push(send_back.str());
		    send_back.str("");
		}
	    }

	    std::cout << "Forwarding to server message queue.\n" << std::endl;

	    std::stringstream msg_to_send;

	    while(!cliMsgQueue.empty())
	    {
		msg_to_send << cliMsgQueue.front();
		cliMsgQueue.pop();

		pthread_mutex_lock(server_queue_mutex);
		(*serverQueue).push(msg_to_send.str());
		pthread_mutex_unlock(server_queue_mutex);

		msg_to_send.str("");
	    }
	}

        boost::asio::async_read_until(socket_, data_buffer, ETX,
	    boost::bind(&socket_state::handle_read, shared_from_this(), 
	    boost::asio::placeholders::error,
	    boost::asio::placeholders::bytes_transferred));
    }
    else if(e == boost::asio::error::eof)
    {
	std::cerr << "WARNING: client forcibly disconnected" << std::endl;
	close();
    }
}

/*
 * Creates a new shared socket state pointer. Necessary for accepting connections.
 */
socket_state::pointer socket_state::create(boost::asio::io_service & io_service)
{
    return socket_state::pointer(new socket_state(io_service));
}

/*
 * Returns the socket in the socket state object.
 */
socket_state::tcp_socket & socket_state::socket()
{
    return socket_;
}

/*
 * Starts the connection process asynchronously.
 */
void socket_state::start()
{
    boost::asio::async_read_until(socket_, data_buffer, ETX, 
	    boost::bind(&socket_state::handle_read, shared_from_this(), 
	    boost::asio::placeholders::error,
	    boost::asio::placeholders::bytes_transferred));
}

