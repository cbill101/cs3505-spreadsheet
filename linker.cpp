#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include "linker.h"

using namespace std;

linker::linker () : sheetNames()
{
    path getcwd(boost::filesystem::current_path());
    boost::system::error_code ec;

    for (boost::filesystem::directory_iterator it (getcwd, ec), eit;
         it != eit;
         it.increment (ec)
         )
    {
	if(ec)
	    continue;

	if (boost::filesystem::is_regular_file (it->path ()))            
        {
            if(it->path ().extension ().string () == ".txt")
	    {
		std::cout << "Found spreadsheet " << it->path().stem().string() << std::endl;
                sheetNames.push_back(it->path().stem().string());
	    }
        }	
    }

}

string linker::decode (std::string message, std::string sheet)
{
    vector<string> tokens;
    boost::split(tokens, message, boost::is_any_of(" "));

    //register
    if(!tokens[0].compare("register"))
    {
	std::stringstream msg;

	msg << "connect_accepted ";

	std::vector<std::string>::iterator file_iter;
	for(file_iter = sheetNames.begin(); file_iter != sheetNames.end(); file_iter++)
	{
	    msg << (*file_iter) + "\n";
	}

	std::string final = msg.str();
	if(final[final.size() - 1] == ' ')
	{
	    return final + static_cast<char>(3);
	}
	else if(final[final.size() - 1] == '\n')
	{
	    final = final.substr(0, final.size() - 1);
	    return final + static_cast<char>(3);
	}
    }

    //load
    if(!tokens[0].compare("load"))
    {
	std::cout << "Got msg. Requested sheet: " << sheet << std::endl;
	if(spreadsheetList.find(sheet) == spreadsheetList.end() )
	{
	    spreadsheetList[sheet] = spreadsheet(sheet);

	    if(std::find(sheetNames.begin(), sheetNames.end(), sheet) == sheetNames.end())
		sheetNames.push_back(sheet);
	}
    
	map<string,string> currentSpreadsheet= spreadsheetList[sheet].getSpreadsheet();
	string toreturn = "full_state ";

	for (map<string, string>::iterator it = currentSpreadsheet.begin(); it != currentSpreadsheet.end(); it++)
	{
	    toreturn += it->first + ":" + it -> second + "\n";
	}
	toreturn += char(3);

	return toreturn;    
    }

    //edit
    if(!tokens[0].compare("edit"))
    {
	vector<string> split_data;
	boost::split(split_data, message, boost::is_any_of(":"));

	string cell = split_data[0].substr(5);
	string contents = message.substr(message.find(':') + 1, message.size() - 1);

	std::cout << contents << std::endl;

	spreadsheetList[sheet].editCell(cell, contents);

	spreadsheetList[sheet].saveFile(spreadsheetList[sheet]);

	std::stringstream msg;

	msg << "change " << cell << ":" << contents << static_cast<char>(3);

	std::string final = msg.str();
	
	return final;
    }

    //undo
    if(!tokens[0].compare("undo"))
    {
	pair< string, string > toreturn = spreadsheetList[sheet].undo();
	if(toreturn.first == "")
	{
	    return "";
	}

	spreadsheetList[sheet].saveFile(spreadsheetList[sheet]);

	std::stringstream msg;
	msg << "change " << toreturn.first << ":" << toreturn.second << static_cast<char>(3);
	std::string final = msg.str();
	return final;
    }

    //revert
    if(!tokens[0].compare("revert"))
    {
	string toreturn = spreadsheetList[sheet].revert(tokens[1]);
	spreadsheetList[sheet].saveFile(spreadsheetList[sheet]);
	return "change "+tokens[1] +":" + toreturn+ static_cast<char>(3);
    }
    return "";
}

std::vector<std::string> linker::getSheetNames()
{
    path getcwd(boost::filesystem::current_path());
    boost::system::error_code ec;
    std::vector<std::string> sheetNames;

    for (boost::filesystem::directory_iterator it (getcwd, ec), eit;
         it != eit;
         it.increment (ec)
         )
    {
	if(ec)
	    continue;

	if (boost::filesystem::is_regular_file (it->path ()))            
        {
            if(it->path ().extension ().string () == "txt")
                sheetNames.push_back(it->path ().stem().string());
        }	
    }

    return sheetNames;
}
