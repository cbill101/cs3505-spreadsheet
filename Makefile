program: spreadsheet.o linker.o socket_state.o tcp_server.o main.o
	g++ -o a.out spreadsheet.o linker.o socket_state.o tcp_server.o main.o -pthread /usr/local/lib/libboost_system.a /usr/local/lib/libboost_filesystem.a

spreadsheet.o: spreadsheet.h spreadsheet.cpp
	g++ -c spreadsheet.cpp

linker.o: linker.h spreadsheet.h linker.cpp
	g++ -c linker.cpp

main.o: tcp_server.h main.cpp
	g++ -c main.cpp

socket_state.o: socket_state.h socket_state.cpp
	g++ -c socket_state.cpp

tcp_server.o: linker.h socket_state.h tcp_server.h tcp_server.cpp
	g++ -c tcp_server.cpp
