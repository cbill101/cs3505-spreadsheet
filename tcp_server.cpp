#include "tcp_server.h"
#include <boost/algorithm/string.hpp>

/*
 * Sends a ping message to all clients every 10 seconds.
 * Runs on its own thread.
 */
void tcp_server::send_ping()
{
    while(server_is_up)
    {
	sleep(10);

	std::stringstream p_stream;
	p_stream << "ping " << static_cast<char>(3);
	std::string ping = p_stream.str();

	std::map<std::string, socket_state::pointer>::iterator client_iter;
	pthread_mutex_lock(&cli_mutex);
	for(client_iter = clients.begin(); client_iter != clients.end(); client_iter++)
	{
	    if(client_iter->second->finished_handshake)
	    {
		client_iter->second->addPingTick();

		if(client_iter->second->getPingTicks() == 6)
		{
		    std::cout << "Client " << client_iter->first << " disconnected (timeout)" << std::endl;
		    if(!client_iter->second->closed)
		    {
			client_iter->second->close();
		    }
		}
		else
		{
		    if(!client_iter->second->closed)
		    {
			client_iter->second->send(ping);
		    }
		    else
		    {
			clients.erase(client_iter++);
		    }
		}
	    }
	}

	if(clients.size() > 0 && server_is_up)
	    std::cout << "Sent ping to all clients." << std::endl;

	pthread_mutex_unlock(&cli_mutex);
    }

    std::cout << "Ping thread closed successfully." << std::endl;
}

/*
 * Nottifies opther clients of the passed in message. Sends to all clients.
 * Can exclude the sender from receiving the message if necessary.
 */
void tcp_server::notify_other_clients(std::string & orig_id, std::string & msg, bool exclude_self)
{
    std::map<std::string, socket_state::pointer>::iterator client_iter;
    pthread_mutex_lock(&cli_mutex);
    for(client_iter = clients.begin(); client_iter != clients.end(); client_iter++)
    {
	if(!(client_iter->second->closed) && (client_iter->second->assigned_sheet == clients[orig_id]->assigned_sheet))
	{
	    try
	    {
		if(exclude_self)
		{
		    if(client_iter->first == orig_id)
			continue;

		    client_iter->second->send(msg);
		}
		else
		    client_iter->second->send(msg);
	    }
	    catch (const std::exception & e)
	    {
		clients.erase(client_iter++);
	    }
	}
    }
    pthread_mutex_unlock(&cli_mutex);
}

/*
 * Processes all messages in the current queue.
 * Runs every 20 ms if the queue is empty.
 * Runs on its own thread.
 */
void tcp_server::process_client_messages()
{
    while(server_is_up)
    {
	sleep(0.002);
	while(!msgQueue.empty())
	{
	    pthread_mutex_lock(&queue_mutex);
	    std::string msg = msgQueue.front();
	    msgQueue.pop();
	    pthread_mutex_unlock(&queue_mutex);

	    char ETX = static_cast<char>(3);

	    std::vector<std::string> split_data;
	    boost::split(split_data, msg, boost::is_any_of("\r"));

	    std::stringstream ID(split_data[0]);

	    std::cout << msg << " received from client " << ID.str() << std::endl;;

	    std::string state_ID = ID.str();

	    std::stringstream send_back;

	    if(split_data[1][split_data[1].size() - 1] == ETX)
	    {	    
		if(split_data[1] == "register \u0003")
		{
		    send_back << sheet_linker.decode(split_data[1], "");
		}
		else if(split_data[1] == "disconnect \u0003")
		{
		    // Client disconnects willingly. Close socket, clean up socket state.
		    pthread_mutex_lock(&cli_mutex);
		    clients[ID.str()]->close();
		    pthread_mutex_unlock(&cli_mutex);
		    continue;
		}
		else if(split_data[1] == "ping \u0003")
		{
		    send_back << "ping_response " << ETX;
		}
		else if(split_data[1] == "ping_response \u0003")
		{
		    pthread_mutex_lock(&cli_mutex);
		    clients[ID.str()]->resetPingTicks();
		    pthread_mutex_unlock(&cli_mutex);
		    continue;
		}
		else if(split_data[1] == "undo \u0003")
		{
		    pthread_mutex_lock(&cli_mutex);		    
		    send_back << sheet_linker.decode(split_data[1], clients[ID.str()]->assigned_sheet);
		    pthread_mutex_unlock(&cli_mutex);

		    std::string final = send_back.str();

		    if(final == "")
			continue;

		    std::cout << "Undo msg: " << final << std::endl;
		    notify_other_clients(split_data[0], final, false);
		    continue;
		}
		else if(split_data[1] == "unfocus \u0003")
		{
		    send_back << "unfocus " << ID.str() << ETX;
		    std::string final = send_back.str();
		    notify_other_clients(split_data[0], final, true);

		    std::cout << "Notified other clients of the focus change." << std::endl;
		    continue;
		}
		else
		{
		    std::string msg_data = split_data[1].substr(0, split_data[1].size() - 1);
		    // Got a revert message. Deal as below.
		    if(msg_data[0] == 'r')
		    {
			pthread_mutex_lock(&cli_mutex);
			send_back << sheet_linker.decode(msg_data, clients[ID.str()]->assigned_sheet);
			pthread_mutex_unlock(&cli_mutex);
		    
			std::string sent_msg = send_back.str();

			std::cout << "Sending msg: " << sent_msg << std::endl;

			notify_other_clients(split_data[0], sent_msg, false);

			std::cout << "Notified other clients of the change." << std::endl;
			continue;
		    }
		    // Got an edit message. Deal as below.
		    if(msg_data[0] == 'e')
		    {	
			pthread_mutex_lock(&cli_mutex);
			send_back << sheet_linker.decode(msg_data, clients[ID.str()]->assigned_sheet);
			pthread_mutex_unlock(&cli_mutex);
		    
			std::string sent_msg = send_back.str();

			std::cout << "Sending msg: " << sent_msg << std::endl;

			notify_other_clients(split_data[0], sent_msg, true);

			std::cout << "Notified other clients of the change." << std::endl;
			continue;
		    }
		    // Got a focus message. Deal as below.
		    if(msg_data[0] == 'f')
		    {
			std::string cell = msg_data.substr(6);
			send_back << "focus " << cell << ":" << ID.str() << ETX;
			std::string msg = send_back.str();

			std::cout << msg << std::endl;
			notify_other_clients(split_data[0], msg, true);

			userCellMap[split_data[0]] = cell;

			std::cout << "Notified other clients of the focus change." << std::endl;
			continue;
		    }
		    // Got a load message. Deal as below. Send full state and current focus from active users.
		    if(msg_data[0] == 'l')
		    {
			std::string sheet_name = msg_data.substr(5);

			pthread_mutex_lock(&cli_mutex);
			send_back << sheet_linker.decode(msg_data + ETX, sheet_name);
			std::string full_state = send_back.str();
			clients[ID.str()]->assigned_sheet = sheet_name;
			clients[ID.str()]->finished_handshake = true;
			clients[ID.str()]->send(full_state);

			sleep(1);

			std::stringstream focus_msg;
			std::map<std::string, std::string>::iterator active_user_iter;
			for(active_user_iter = userCellMap.begin(); active_user_iter != userCellMap.end();)
			{
			    if(clients.find(active_user_iter->first) == clients.end())
			    {
				userCellMap.erase(active_user_iter++);
			    }
			    else
			    {
				if(clients[active_user_iter->first]->assigned_sheet == sheet_name && active_user_iter->first != state_ID)
				{
				    std::stringstream focusmsg;
				    focusmsg << "focus " << active_user_iter->second << ":" << active_user_iter->first << ETX;
				    std::string msg = focusmsg.str();

				    std::cout << msg << std::endl;
				    clients[state_ID]->send(msg);
				}

				++active_user_iter;
			    }
			}

			pthread_mutex_unlock(&cli_mutex);

			std::cout << "Giving full state of desired sheet to client " << ID.str() << ": " << full_state << std::endl;
			
			continue;
		    }
		}
	    }

	    std::string final_msg = send_back.str();

	    std::cout << "Sent msg to client " << ID.str() << ": " << final_msg << std::endl;

	    pthread_mutex_lock(&cli_mutex);
	    clients[ID.str()]->send(final_msg);
	    pthread_mutex_unlock(&cli_mutex);
	}
    }

    std::cout << "All messsages processed successfully. Queue thread closed." << std::endl;
}

/*
 * Runs through the client list and removes all disconnected clients from the list of clients every 200 ms.
 * Runs on its own thread.
 */
void tcp_server::clean_clients()
{
    while(server_is_up)
    {
	sleep(0.2);
	pthread_mutex_lock(&cli_mutex);
	std::map<std::string, socket_state::pointer>::iterator client_iter;
	for(client_iter = clients.begin(); client_iter != clients.end(); )
	{
	    if(client_iter->second->closed)
	    {
		std::cout << "Client " << client_iter->first << " removed from client list." << std::endl;
		clients.erase(client_iter++);
	    }
	    else
	    {
		++client_iter;
	    }
	}
	pthread_mutex_unlock(&cli_mutex);
    }

    std::cout << "\nDone cleaning the list of clients" << std::endl;
}

/*
 * Static helper method to assign a thread to send pings.
 */
static void* static_send_ping(void* p)
{
    static_cast<tcp_server*>(p)->send_ping();
    return NULL;
}

/*
 * Static helper method to assign a thread to process client messages.
 */
static void* static_send_queue_data(void* p)
{
    static_cast<tcp_server*>(p)->process_client_messages();
    return NULL;
}

/*
 * Static helper method to assign a thread to clean up the client list.
 */
static void* static_clean_clients(void* p)
{
    static_cast<tcp_server*>(p)->clean_clients();
    return NULL;   
}

/*
 * Constructor:
 * Creates a new tcp_server object. Initializes necessary mutex objects and threads, and other objects to run the collaborative spreadsheet server.
 * Args:
 * io_service: The asio service that accepts clients and manages async operations.
 */
tcp_server::tcp_server(boost::asio::io_service & io_service)
    : acceptor_(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 2112)),
      clients(), task_threads(), msgQueue(), sheet_linker(), userCellMap()
{
    nextClientID = 0;

    server_is_up = true;
    
    pthread_t ping_thread;
    int rc = pthread_create(&ping_thread, NULL, static_send_ping, this);
    task_threads.push_back(ping_thread);
    pthread_mutex_init(&cli_mutex, NULL);

    pthread_t cleaning_thread;
    rc = pthread_create(&cleaning_thread, NULL, static_clean_clients, this);
    task_threads.push_back(cleaning_thread);

    pthread_t queue_thread;
    rc = pthread_create(&queue_thread, NULL, static_send_queue_data, this);
    task_threads.push_back(queue_thread);
    pthread_mutex_init(&queue_mutex, NULL);

    start_accept();
}

/*
 * Async callback upon new client connecting.
 * Handles the initial connection, starts the socket's async processes,
 * and gives the socket all necessary information for proper functionality.
 * Args:
 * sock_state: A boost::shared_ptr to an object containing a socket, buffer, ID, etc.
 * error: The boost error code if any.
 */
void tcp_server::handle_first_contact(socket_state::pointer & sock_state,
				      const boost::system::error_code & error)
{
    if (!error)
    {
	std::cout << "New client connected to server: " << nextClientID << std::endl;

	(*sock_state).assignServerInfo(msgQueue, queue_mutex);

	std::stringstream ID;
	ID << nextClientID;

	(*sock_state).ss_ID = ID.str();

	nextClientID++;

	(*sock_state).start();

	pthread_mutex_lock(&cli_mutex);
	clients[(*sock_state).ss_ID] = sock_state;
	pthread_mutex_unlock(&cli_mutex);
    }

    // Call start_accept() to initiate the next accept operation.
    start_accept();
}

/*
 * Accepts a new connection from a client.
 */
void tcp_server::start_accept()
{
    // creates a socket
    socket_state::pointer new_sock_state =
      socket_state::create(acceptor_.get_io_service());

    // initiates an asynchronous accept operation 
    // to wait for a new connection. 
    acceptor_.async_accept(new_sock_state->socket(),
        boost::bind(&tcp_server::handle_first_contact, this, new_sock_state,
		    boost::asio::placeholders::error));
}

/*
 * Closes the server. Cleans up all running threads,
 * processes all remaining messages in the message queue,
 * disocnnects all clients, and destroys the mutex objects.
 */
void tcp_server::close()
{
    server_is_up = false;

    pthread_join(task_threads[1], NULL);
    pthread_join(task_threads[2], NULL);

    std::vector<pthread_t>::iterator thread_iter;

    std::map<std::string, socket_state::pointer>::iterator client_iter;
    
    std::stringstream disconnect_msg;
    disconnect_msg << "disconnect " << static_cast<char>(3);
    std::string disconnect = disconnect_msg.str();

    pthread_mutex_lock(&cli_mutex);
    for(client_iter = clients.begin(); client_iter != clients.end(); client_iter++)
    {
	if(!client_iter->second->closed)
	{
	    try
	    {
		client_iter->second->send(disconnect);
		client_iter->second->close();
	    }
	    catch (std::exception & ex)
	    {
		client_iter->second->close();
	    }
	}
    }

    std::cout << "Disconnected all clients successfully." << std::endl;

    clients.clear();
    pthread_mutex_unlock(&cli_mutex);

    pthread_join(task_threads[0], NULL);  

    pthread_mutex_destroy(&cli_mutex);
    pthread_mutex_destroy(&queue_mutex);
}
