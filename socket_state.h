#ifndef SOCKET_STATE_H
#define SOCKET_STATE_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <pthread.h>
#include <iostream>
#include <string>
#include <queue>

class socket_state
  : public boost::enable_shared_from_this<socket_state>
{
public:
  typedef boost::shared_ptr<socket_state> pointer;
  typedef boost::asio::ip::tcp::socket tcp_socket;

  static pointer create(boost::asio::io_service& io_service);

  tcp_socket & socket();

  std::string assigned_sheet;

  std::string ss_ID;

  bool closed;

  void start();

  void send(std::string &);

  void addPingTick();

  int getPingTicks();

  void resetPingTicks();

  void close();

  void assignServerInfo(std::queue<std::string> &, pthread_mutex_t &);

  std::queue<std::string> * serverQueue;

  std::queue<std::string> cliMsgQueue;

  bool finished_handshake;

  pthread_mutex_t * server_queue_mutex;

  tcp_socket socket_;

private:
  socket_state(boost::asio::io_service& io_service);

  void handle_read(const boost::system::error_code&, size_t);

  // handle_write() is responsible for any further actions 
  // for this client connection.
  void handle_write();

  // The buffer holding the socket's incoming data
  boost::asio::streambuf data_buffer;
  // ASCII: 3. End of text. Terminates a message.
  std::string ETX;
  // Keep track of when the socket received a ping.
  int ping_ticks;
};

#endif
