﻿Author: Chris Billingsley
UID: u1041095
Date: 10/27/17
Changes:
I made a ton of GitHub commits, it was ridiculous. I'm descriptive with them, so check there.
Also, forgot about this section of the README.

This project brings together the entire program to bring about a fully functioning Spreadsheet program, with GUI and spreadsheet logic.

Reflection:
Time this took: 15-20 hours (majority of which I expanded on the sheet when I was finished with the basic specifications)

GUI's are awesome, yet a pain. After working with it long enough though, and modifying the SpreadsheetPanel as well, I feel like I
have a deeper understanding of GUI's. I didn't choose a partner for this assignment because I feel like I learn best on my own.
Also, just past bad experiences. CS 2420 gives me flashbacks on partners... it was bad.

Of course, that will change for when the multiplayer game needs to be implemented. But, that's another challenge to be dealt with another day.

Grading Notes:
Scrolling with the mouse wheel is wonky. I talked to Josh about it, and said it's not a big deal.
The mouse wheel works when scrolling up and down. On laptops, though, do not use the touchpad to scroll left and right.
You have been warned! (It gets processed as arrow keys, left and right)

Also, the SpreadsheetPanel has been modified with handling arrow key nagivation and hotkey detection.
(Basically, a glorified KeyDown event that helps with nagivation and shortcuts for the entire program.)

Otherwise, I'm happy with what's produced.