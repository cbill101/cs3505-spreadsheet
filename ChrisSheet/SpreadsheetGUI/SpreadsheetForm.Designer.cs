﻿namespace SpreadsheetGUI
{
    partial class SpreadsheetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadsheetForm));
            this.spreadsheetMenuStrip = new System.Windows.Forms.MenuStrip();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuTab = new System.Windows.Forms.ToolStripMenuItem();
            this.newSheetTab = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTab = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTab = new System.Windows.Forms.ToolStripMenuItem();
            this.cellNameLabel = new System.Windows.Forms.Label();
            this.cellValueLabel = new System.Windows.Forms.Label();
            this.cellValueTextBox = new System.Windows.Forms.TextBox();
            this.cellInformationPanel = new System.Windows.Forms.Panel();
            this.currentCellNameLabel = new System.Windows.Forms.Label();
            this.currentContentsTextBox = new System.Windows.Forms.TextBox();
            this.currentContentsLabel = new System.Windows.Forms.Label();
            this.SSPanel = new SS.SpreadsheetPanel();
            this.disconnectTab = new System.Windows.Forms.ToolStripMenuItem();
            this.spreadsheetMenuStrip.SuspendLayout();
            this.cellInformationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // spreadsheetMenuStrip
            // 
            this.spreadsheetMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.spreadsheetMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem,
            this.fileMenuTab,
            this.editToolStripMenuItem,
            this.helpTab});
            this.spreadsheetMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.spreadsheetMenuStrip.Name = "spreadsheetMenuStrip";
            this.spreadsheetMenuStrip.Size = new System.Drawing.Size(982, 24);
            this.spreadsheetMenuStrip.TabIndex = 0;
            this.spreadsheetMenuStrip.Text = "menuStrip1";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.connectToolStripMenuItem.Text = "Connect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.ConnectTabClick);
            // 
            // fileMenuTab
            // 
            this.fileMenuTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSheetTab,
            this.disconnectTab,
            this.closeTab});
            this.fileMenuTab.Name = "fileMenuTab";
            this.fileMenuTab.Size = new System.Drawing.Size(37, 20);
            this.fileMenuTab.Text = "File";
            // 
            // newSheetTab
            // 
            this.newSheetTab.Name = "newSheetTab";
            this.newSheetTab.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.newSheetTab.Size = new System.Drawing.Size(180, 22);
            this.newSheetTab.Text = "Load";
            this.newSheetTab.Click += new System.EventHandler(this.LoadTabClick);
            // 
            // closeTab
            // 
            this.closeTab.Name = "closeTab";
            this.closeTab.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.closeTab.Size = new System.Drawing.Size(180, 22);
            this.closeTab.Text = "Close Sheet";
            this.closeTab.Click += new System.EventHandler(this.CloseTabClick);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.revertToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // revertToolStripMenuItem
            // 
            this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
            this.revertToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.revertToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.revertToolStripMenuItem.Text = "Revert";
            this.revertToolStripMenuItem.Click += new System.EventHandler(this.revertToolStripMenuItem_Click);
            // 
            // helpTab
            // 
            this.helpTab.Name = "helpTab";
            this.helpTab.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.helpTab.Size = new System.Drawing.Size(44, 20);
            this.helpTab.Text = "Help";
            this.helpTab.Click += new System.EventHandler(this.HelpTabClick);
            // 
            // cellNameLabel
            // 
            this.cellNameLabel.AutoSize = true;
            this.cellNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellNameLabel.Location = new System.Drawing.Point(3, 11);
            this.cellNameLabel.Name = "cellNameLabel";
            this.cellNameLabel.Size = new System.Drawing.Size(43, 13);
            this.cellNameLabel.TabIndex = 2;
            this.cellNameLabel.Text = "Name:";
            // 
            // cellValueLabel
            // 
            this.cellValueLabel.AutoSize = true;
            this.cellValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellValueLabel.Location = new System.Drawing.Point(91, 11);
            this.cellValueLabel.Name = "cellValueLabel";
            this.cellValueLabel.Size = new System.Drawing.Size(43, 13);
            this.cellValueLabel.TabIndex = 4;
            this.cellValueLabel.Text = "Value:";
            // 
            // cellValueTextBox
            // 
            this.cellValueTextBox.Location = new System.Drawing.Point(134, 8);
            this.cellValueTextBox.Name = "cellValueTextBox";
            this.cellValueTextBox.ReadOnly = true;
            this.cellValueTextBox.Size = new System.Drawing.Size(77, 20);
            this.cellValueTextBox.TabIndex = 8;
            this.cellValueTextBox.TabStop = false;
            // 
            // cellInformationPanel
            // 
            this.cellInformationPanel.Controls.Add(this.currentCellNameLabel);
            this.cellInformationPanel.Controls.Add(this.currentContentsTextBox);
            this.cellInformationPanel.Controls.Add(this.currentContentsLabel);
            this.cellInformationPanel.Controls.Add(this.cellNameLabel);
            this.cellInformationPanel.Controls.Add(this.cellValueTextBox);
            this.cellInformationPanel.Controls.Add(this.cellValueLabel);
            this.cellInformationPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.cellInformationPanel.Location = new System.Drawing.Point(0, 24);
            this.cellInformationPanel.Name = "cellInformationPanel";
            this.cellInformationPanel.Size = new System.Drawing.Size(982, 35);
            this.cellInformationPanel.TabIndex = 9;
            // 
            // currentCellNameLabel
            // 
            this.currentCellNameLabel.AutoSize = true;
            this.currentCellNameLabel.Location = new System.Drawing.Point(52, 11);
            this.currentCellNameLabel.Name = "currentCellNameLabel";
            this.currentCellNameLabel.Size = new System.Drawing.Size(0, 13);
            this.currentCellNameLabel.TabIndex = 12;
            // 
            // currentContentsTextBox
            // 
            this.currentContentsTextBox.Location = new System.Drawing.Point(284, 8);
            this.currentContentsTextBox.Name = "currentContentsTextBox";
            this.currentContentsTextBox.ReadOnly = true;
            this.currentContentsTextBox.Size = new System.Drawing.Size(686, 20);
            this.currentContentsTextBox.TabIndex = 11;
            this.currentContentsTextBox.TabStop = false;
            // 
            // currentContentsLabel
            // 
            this.currentContentsLabel.AutoSize = true;
            this.currentContentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentContentsLabel.Location = new System.Drawing.Point(217, 11);
            this.currentContentsLabel.Name = "currentContentsLabel";
            this.currentContentsLabel.Size = new System.Drawing.Size(61, 13);
            this.currentContentsLabel.TabIndex = 10;
            this.currentContentsLabel.Text = "Contents:";
            // 
            // SSPanel
            // 
            this.SSPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SSPanel.Location = new System.Drawing.Point(0, 58);
            this.SSPanel.Name = "SSPanel";
            this.SSPanel.Size = new System.Drawing.Size(982, 543);
            this.SSPanel.TabIndex = 1;
            this.SSPanel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SSPanel_KeyDown);
            this.SSPanel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSPanel_KeyPress);
            this.SSPanel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.SSPanel_MouseDoubleClick);
            // 
            // disconnectTab
            // 
            this.disconnectTab.Name = "disconnectTab";
            this.disconnectTab.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.disconnectTab.Size = new System.Drawing.Size(180, 22);
            this.disconnectTab.Text = "Disconnect";
            this.disconnectTab.Click += new System.EventHandler(this.disconnectTab_Click);
            // 
            // SpreadsheetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(982, 601);
            this.Controls.Add(this.cellInformationPanel);
            this.Controls.Add(this.spreadsheetMenuStrip);
            this.Controls.Add(this.SSPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.spreadsheetMenuStrip;
            this.Name = "SpreadsheetForm";
            this.Text = "Spreadsheet";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpreadsheetForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SpreadsheetForm_KeyDown);
            this.spreadsheetMenuStrip.ResumeLayout(false);
            this.spreadsheetMenuStrip.PerformLayout();
            this.cellInformationPanel.ResumeLayout(false);
            this.cellInformationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip spreadsheetMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenuTab;
        private System.Windows.Forms.ToolStripMenuItem newSheetTab;
        private System.Windows.Forms.ToolStripMenuItem closeTab;
        private System.Windows.Forms.ToolStripMenuItem helpTab;
        private System.Windows.Forms.Label cellNameLabel;
        private System.Windows.Forms.Label cellValueLabel;
        private System.Windows.Forms.TextBox cellValueTextBox;
        private System.Windows.Forms.Panel cellInformationPanel;
        private System.Windows.Forms.TextBox currentContentsTextBox;
        private System.Windows.Forms.Label currentContentsLabel;
        private SS.SpreadsheetPanel SSPanel;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.Label currentCellNameLabel;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disconnectTab;
    }
}

