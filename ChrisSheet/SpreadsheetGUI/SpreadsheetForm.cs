﻿// ===============================
// AUTHOR     : Chris Billingsley
// UID        : u1041095
// CLASS      : CS 3500
// PURPOSE    : To serve as the GUI, house events, and manage overall flow of the program.
// NOTES      : None.
// ===============================
// Change History:
// See Git commits for details.
// ==================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SS;
using SpreadsheetUtilities;
using System.IO;
using static Controller.NetworkController;
using Controller;
using System.Threading;

namespace SpreadsheetGUI
{
    /// <summary>
    /// Represents a SpreadsheetForm, with functions to navigate the GUI and modify a Spreadsheet object.
    /// </summary>
    public partial class SpreadsheetForm : Form
    {
        #region Variables

        // The backbone of the entire program... the spreadsheet itself.
        private Spreadsheet spreadsheet;


        // The connect form that needs to close after connection
        private ConnectForm connectForm;

        // The default cell that the spreadsheet program starts at.
        private const string DEFAULT_CELL = "A1";

        // The end of text character
        private string ETX = "" + (char)3;

        // modifiedCellStack keeps track of the modified cells for undoing.
        // cellContentsStack keeps track of the cell's old contents for undoing.
        private Stack<string> modifiedCellStack;
        private Stack<object> cellContentsStack;

        private Dictionary<string, Tuple<int, int>> otherUsers = new Dictionary<string, Tuple<int, int>>();

        //Keeps track of last row and column of edited cell
        private int lastRow, lastCol;

        //Keeps track of the name of the last edited cell
        private string lastCellName;

        //Typed input
        private StringBuilder input;

        //Timer for ping loop
        private System.Windows.Forms.Timer pingTimer;

        // Contains information for server
        private SocketState serverSocket;

        //Counter for missed pings
        private int pingMiss = 0;

        /// <summary>
        /// A value that says if the client is connected to a server currently.
        /// </summary>
        public bool OnActiveSpreadsheet { get; private set; }

        /// <summary>
        /// The current connected server's hostname.
        /// </summary>
        public string CurrentServerHostname { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new SpreadsheetForm, with a menu, cell information panel, and SpreadsheetPanel.
        /// </summary>
        public SpreadsheetForm()
        {
            InitializeComponent();

            spreadsheet = new Spreadsheet(s => true, s => s, "PS6");

            SSPanel.Enabled = false;

            lastRow = -1;
            lastCol = -1;

            lastCellName = DEFAULT_CELL;

            modifiedCellStack = new Stack<string>();
            cellContentsStack = new Stack<object>();

            SSPanel.SelectionChanged += UpdateSelection;

            SSPanel.KeybindDown += SSPanel_KeyDown;

            SSPanel.TextBoxInput += HandleTextBoxInput;

            SSPanel.TextBoxSelChanged += HandleTextBoxSelectionChange;

            SSPanel.CellDoubleClick += HandleInitialDoubleClick;

            input = new StringBuilder();
            // Focus goes to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];

            editToolStripMenuItem.Enabled = false;

            // Convert from char int values to Spreadsheet coordinates.
            SSPanel.SetSelection(DEFAULT_CELL[0] - 65, int.Parse("" + DEFAULT_CELL[1]) - 1);
            UpdateSelection(SSPanel);
        }

        #endregion

        #region Spreadsheet

        #region Spreadsheet Interaction

        /// <summary>
        /// Fills intial spreadsheet with full state from server
        /// </summary>
        /// <param name="state"></param>
        public void FillNewSpreadsheet(SocketState state)
        {
            SSPanel.Clear();

            SSPanel.ClearActiveUsers();

            input.Clear();
            //Splits initial message in case multiple messages are sent
            String[] message = state.sb.ToString().Split(new string[] { ETX }, StringSplitOptions.None);

            Invoke(new Action(() => connectToolStripMenuItem.Enabled = false));

            string[] cmd = message[0].Split(new string[] { " " }, StringSplitOptions.None);

            //Checks to see if the first message is correct
            if (cmd[0] == "full_state")
            {
                string msgContnt = message[0].Substring(11, message[0].Length - 11);
                //Dictionary containing a cell's name and its respective contents

                Dictionary<string, string> cellChanges = new Dictionary<string, string>();

                spreadsheet = new Spreadsheet();

                //Splits each edit
                String[] edits = msgContnt.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                //Checks to see if there are no edits
                if (edits.Count() != 0)
                {
                    for (int count = 0; count < edits.Count(); count++)
                    {
                        String[] cells = edits[count].Split(new string[] { ":" }, StringSplitOptions.None);
                        cellChanges.Add(cells[0], cells[1]);
                    }

                    foreach (KeyValuePair<string, string> edit in cellChanges)
                    {
                        string cellname = edit.Key;
                        Tuple<int, int> location = CellToLocation(cellname);

                        lock(SSPanel)
                        {
                            SSPanel.SetValue(location.Item1, location.Item2, edit.Value);
                        }

                        InsertContents(location.Item1, location.Item2, edit.Value, true);
                    }
                }
                //Allows editing on spreadsheet
                SSPanel.BeginInvoke(new Action(() => SSPanel.Enabled = true));
                //Start ping loop
                Invoke(new Action(() =>
                {
                    InitTimer();
                    editToolStripMenuItem.Enabled = true;
                }));

                //Set Callback
                serverSocket.ClientCallback = ReceiveAndSendMessage;

                //Clear string builder
                serverSocket.sb.Clear();

                OnActiveSpreadsheet = true;

                SSPanel.GetSelection(out int col, out int row);

                Networking.SendSocket(serverSocket.theSocket, "focus " + LocationToCell(col, row) + ETX);

                //Starts server message loop
                Networking.GetData(serverSocket);
            }
            //Error opening or making sheet
            else if (cmd[0] == "file_load_error")
            {
                MessageBox.Show("Unable to load or create requested spreadsheet");
            }
            //An invalid message was sent
            else
            {
                MessageBox.Show("Invalid response from server");
            }
        }

        /// <summary>
        /// Receieves and sends messages to client
        /// </summary>
        /// <param name="ss"></param>
        private void ReceiveAndSendMessage(SocketState ss)
        {
            //Splits initial message in case multiple messages are sent

            String[] message = ss.sb.ToString().Split(new string[] { ETX }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in message)
            {
                //Splits initial command
                String[] command = s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                Tuple<int, int> location;

                if (command.Length > 0)
                {
                    //Handles all messages
                    switch (command[0])
                    {
                        case "disconnect":
                            ss = null;
                            serverSocket = null;
                            //Disables editing on spreadsheet
                            SSPanel.BeginInvoke(new Action(() => SSPanel.Enabled = false));
                            //Reenables connect option
                            Invoke(new Action(() =>
                            {
                                connectToolStripMenuItem.Enabled = true;
                                editToolStripMenuItem.Enabled = false;
                            }));

                            SSPanel.Clear();

                            SSPanel.ClearActiveUsers();

                            break;
                        case "ping":
                            PingResponse(ss);
                            break;

                        case "ping_response":
                            pingMiss = 0;
                            break;

                        case "change":
                            String[] changes = command[1].Split(new string[] { ":" }, StringSplitOptions.None);
                            location = CellToLocation(changes[0]);

                            string content = command[1].Substring(command[1].IndexOf(':') + 1);

                            InsertContents(location.Item1, location.Item2, content, true);

                            break;

                        case "focus":
                            String[] user = command[1].Split(new string[] { ":" }, StringSplitOptions.None);
                            location = CellToLocation(user[0]);

                            SSPanel.AddToActiveCells(user[1], location.Item1, location.Item2);

                            break;

                        case "unfocus":
                            string userID = command[1];

                            SSPanel.RemoveFromActiveCells(userID);

                            break;
                    }
                }
            }
            //Checks to make sure server has not disconnected
            if (ss != null)
            {
                ss.sb.Clear();

                ss.ClientCallback = ReceiveAndSendMessage;
                Networking.GetData(ss);
            }
        }

        /// <summary>
        /// Serves as the selection handler for the program. Changes the cell text values on selection.
        /// </summary>
        /// <param name="sender"></param>
        private void UpdateSelection(SpreadsheetPanel sender)
        {
            sender.GetSelection(out int col, out int row);
            sender.GetValue(col, row, out string val);

            currentCellNameLabel.Text = LocationToCell(col, row);

            if (lastRow != -1 && lastCol != -1)
            {
                if (lastRow != row || lastCol != col)
                {
                    InsertContents(lastCol, lastRow, false);
                }
            }

            lastRow = row;
            lastCol = col;
            lastCellName = GetCellName();

            input.Clear();

            UpdateTextValuesOnSelection(col, row);
        }

        /// <summary>
        /// Helper methodf, called when the text values need to change after the selection changes.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        private void UpdateTextValuesOnSelection(int col, int row)
        {
            SendLocation(col, row);
            currentCellNameLabel.Text = GetCellName();
            SSPanel.GetValue(col, row, out string value);
            cellValueTextBox.Text = value;

            object contents = spreadsheet.GetCellContents(currentCellNameLabel.Text);

            if (!(contents is string || contents is double))
                currentContentsTextBox.Text = "=" + contents.ToString();
            else
                currentContentsTextBox.Text = contents.ToString();
        }

        private void SendLocation(int col, int row)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char letter = letters[col];
            string cellName = letter + (row + 1).ToString();
            cellName = "focus " + cellName + ETX;
            if (serverSocket != null)
            {
                Networking.SendSocket(serverSocket.theSocket, cellName);
            }
        }

        /// <summary>
        /// Helper method, called when the text values need to change after contents of a cell are changed, or when a spreadsheet is opened.
        /// </summary>
        /// <param name="cell"></param>
        private void UpdateTextValuesOnContentChangeOrLoad(string cell)
        {
            int row, col;
            col = cell[0] - 65;
            row = int.Parse(cell.Substring(1)) - 1;
            string val = spreadsheet.GetCellValue(cell).ToString();

            lock(SSPanel)
            {
                SSPanel.SetValue(col, row, val);
            }

            if (cell != currentCellNameLabel.Text)
                return;

            SSPanel.BeginInvoke(new Action(() => cellValueTextBox.Text = val));

            object contents = spreadsheet.GetCellContents(currentCellNameLabel.Text);

            if (!(contents is string || contents is double))
                Invoke(new Action(() => currentContentsTextBox.Text = "=" + contents.ToString()));
            else
                Invoke(new Action(() => currentContentsTextBox.Text = contents.ToString()));
        }

        /// <summary>
        /// Helper method to get the selected cell's name.
        /// </summary>
        /// <returns></returns>
        private string GetCellName()
        {
            SSPanel.GetSelection(out int col, out int row);
            return char.ConvertFromUtf32(col + 65) + (row + 1);
        }

        /// <summary>
        /// Converts a canon cell name to its location on the sheet.
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private Tuple<int, int> CellToLocation(string cell)
        {
            return Tuple.Create(cell[0] - 65, int.Parse(cell.Substring(1)) - 1);
        }

        /// <summary>
        /// Converts a sheet ocation to a canon cell name.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private string LocationToCell(int col, int row)
        {
            return char.ConvertFromUtf32(col + 65) + (row + 1);
        }

        /// <summary>
        /// Deletes the cell. Also adds the contents and cell name to the history stacks.
        /// </summary>
        private void DeleteSelectedCell()
        {
            string cell = GetCellName();
            spreadsheet.SetContentsOfCell(cell, "");
            lock(SSPanel)
            {
                SSPanel.SetValue(lastRow, lastCol, spreadsheet.GetCellValue(cell).ToString());
            }

            UpdateTextValuesOnContentChangeOrLoad(cell);

            modifiedCellStack.Push(cell);

            input.Clear();
        }

        /// <summary>
        /// Helper method to insert cell contents, while updating history stacks and GUI information.
        /// </summary>
        private void InsertContents(int col, int row, string contents, bool isedit)
        {
            // Push cell name and contents onto the history stacks. 
            // Also, get the non empty cells and loop though and change information in the sheet and GUI.
            if (contents != "")
            {
                if (contents[0] == '=')
                {
                    contents = contents.ToUpper();
                }
            }

            HashSet<string> cells;

            try
            {
                cells = new HashSet<string>(spreadsheet.SetContentsOfCell(LocationToCell(col, row), contents));

                foreach (string cell in cells)
                {
                    UpdateTextValuesOnContentChangeOrLoad(cell);

                    if (spreadsheet.GetCellContents(cell) is Formula)
                    {
                        contents = "=" + spreadsheet.GetCellContents(cell).ToString();
                    }
                    else
                    {
                        contents = spreadsheet.GetCellContents(cell).ToString();
                    }

                    if (!isedit)
                        Networking.SendSocket(serverSocket.theSocket, "edit " + cell + ":" + contents + ETX);
                }
            }
            catch (CircularException e)
            {
                foreach (string name in spreadsheet.GetNamesOfAllNonemptyCells())
                {
                    UpdateTextValuesOnContentChangeOrLoad(name);

                    if (spreadsheet.GetCellContents(name) is Formula)
                    {
                        contents = "=" + spreadsheet.GetCellContents(name).ToString();
                    }
                    else
                    {
                        contents = spreadsheet.GetCellContents(name).ToString();
                    }

                    if (!isedit)
                    {
                        //if (spreadsheet.GetCellContents(name) is Formula)
                        //{
                        //    contents = "=" + spreadsheet.GetCellContents(name).ToString();
                        //}
                        //else
                        //{
                        //    contents = spreadsheet.GetCellContents(name).ToString();
                        //}
                        Networking.SendSocket(serverSocket.theSocket, "edit " + name + ":" + contents + ETX);
                    }
                }
            }
            catch (FormulaFormatException e)
            {
                lock(SSPanel)
                {
                    SSPanel.SetValue(col, row, "SpreadsheetUtility.FormulaError");
                }
                Networking.SendSocket(serverSocket.theSocket, "edit " + LocationToCell(col, row) + ":" + contents + ETX);
            }

            // Focus goes back to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];
        }

        /// <summary>
        /// Helper method to insert cell contents, while updating history stacks and GUI information.
        /// </summary>
        private void InsertContents(int col, int row, bool isedit)
        {
            if (input.Length == 0)
                return;

            string contents = input.ToString();

            // Push cell name and contents onto the history stacks. 
            // Also, get the non empty cells and loop though and change information in the sheet and GUI.
            if (contents != "")
            {
                if (contents[0] == '=')
                {
                    contents = contents.ToUpper();
                }
            }

            HashSet<string> cells;

            try
            {
                cells = new HashSet<string>(spreadsheet.SetContentsOfCell(LocationToCell(col, row), contents));

                foreach (string cell in cells)
                {
                    UpdateTextValuesOnContentChangeOrLoad(cell);

                    if (spreadsheet.GetCellContents(cell) is Formula)
                    {
                        contents = "=" + spreadsheet.GetCellContents(cell).ToString();
                    }
                    else
                    {
                        contents = spreadsheet.GetCellContents(cell).ToString();
                    }

                    if (!isedit)
                        Networking.SendSocket(serverSocket.theSocket, "edit " + cell + ":" + contents + ETX);
                }
            }
            catch (CircularException e)
            {
                foreach (string name in spreadsheet.GetNamesOfAllNonemptyCells())
                {
                    UpdateTextValuesOnContentChangeOrLoad(name);

                    if (spreadsheet.GetCellContents(name) is Formula)
                    {
                        contents = "=" + spreadsheet.GetCellContents(name).ToString();
                    }
                    else
                    {
                        contents = spreadsheet.GetCellContents(name).ToString();
                    }

                    if (!isedit)
                        Networking.SendSocket(serverSocket.theSocket, "edit " + name + ":" + contents + ETX);
                }
            }
            catch (FormulaFormatException e)
            {
                lock (SSPanel)
                {
                    SSPanel.SetValue(col, row, "SpreadsheetUtility.FormulaError");
                }
                Networking.SendSocket(serverSocket.theSocket, "edit " + LocationToCell(col, row) + ":" + contents + ETX);
            }

            // Focus goes back to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];

            input.Clear();
        }

        /// <summary>
        /// Sets server's socket for spreadsheet
        /// </summary>
        /// <param name="state"></param>
        public void SetServerSocket(SocketState state)
        {
            serverSocket = state;
        }

        #endregion

        #region Tab Clicks

        /// <summary>
        /// Shows the help dialog.
        /// </summary>
        private void ShowHelpDialog()
        {
            HelpForm helpForm = new HelpForm();
            helpForm.ShowDialog();
        }

        /// <summary>
        /// When Connect is clicked, begins connecting client to server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectTabClick(object sender, EventArgs e)
        {
            connectForm = new ConnectForm(this);
            try
            {
                connectForm.ShowDialog();
            }
            catch (ObjectDisposedException)
            {
                MessageBox.Show("Server not found");
                connectForm.Close();
            }
        }

        /// <summary>
        /// When Load is clicked, pulls up a list of spreadsheets and allows the user to make or load a sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadTabClick(object sender, EventArgs e)
        {
            if (serverSocket == null)
            {
                MessageBox.Show("Please connect to a server first!", "Not Connected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            LoadForm loadForm = new LoadForm(this);
            loadForm.SetServerSocket(serverSocket);
            loadForm.ShowDialog();
        }

        /// <summary>
        /// When File -> Close is clicked, closes the spreadsheet form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseTabClick(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// When Help is clicked, the help window opens, presenting an overview of the program as well as the functions within.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpTabClick(object sender, EventArgs e)
        {
            ShowHelpDialog();
        }

        #endregion

        #region  User Actions

        private void HandleTextBoxInput(TextBox sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SSPanel.GetSelection(out int col, out int row);
                InsertContents(col, row, false);
            }
            else
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (input.Length > 0)
                    {
                        input.Remove(input.Length - 1, 1);
                    }
                }
                else if (!char.IsControl(e.KeyChar))
                {
                    input.Append(e.KeyChar);
                }
            }
        }

        private void HandleTextBoxSelectionChange(TextBox sender, EventArgs e)
        {
            input.Clear();
            input.Append(sender.Text);
            InsertContents(lastCol, lastRow, false);
        }

        private void HandleInitialDoubleClick(TextBox sender, EventArgs e)
        {
            input.Clear();

            if (input.Length == 0)
            {
                object contents = spreadsheet.GetCellContents(currentCellNameLabel.Text);

                if (contents is Formula)
                {
                    sender.Text = "=" + contents.ToString();
                    input.Append(sender.Text);
                }
                else
                {
                    sender.Text = contents.ToString();
                    input.Append(sender.Text);
                }
            }
        }

        /// <summary>
        /// On KeyDown, process key bindings for the Spreadsheet, after navigation is processed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSPanel_KeyDown(object sender, KeyEventArgs e)
        {
            // Delete a cell
            if (e.KeyCode == Keys.Delete)
            {
                SSPanel.GetSelection(out int col, out int row);
                DeleteSelectedCell();
                Networking.SendSocket(serverSocket.theSocket, "edit " + LocationToCell(col, row) + ":" + "" + ETX);
            }

            e.Handled = true;
        }

        /// <summary>
        /// General hotkeys are processed through here after keys are processed through the spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpreadsheetForm_KeyDown(object sender, KeyEventArgs e)
        {
            // Pulls up the help dialog box.
            if (e.KeyData == (Keys.Control | Keys.H))
            {
                ShowHelpDialog();
            }
            // Makes a new speadsheet.
            else if (e.KeyData == (Keys.Control | Keys.N))
            {
                SpreadsheetApplicationContext.getAppContext().RunForm(new SpreadsheetForm());
            }

            // Selects the default cell.
            else if (e.KeyData == (Keys.Control | Keys.Home))
            {
                SSPanel.SetSelection(DEFAULT_CELL[0] - 65, int.Parse("" + DEFAULT_CELL[1]) - 1);
                UpdateSelection(SSPanel);
            }
            // Close the spreadsheet form.
            else if (e.KeyData == (Keys.Alt | Keys.F4))
            {
                Close();
            }
        }

        private void SSPanel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (input.Length == 0)
            {
                object contents = spreadsheet.GetCellContents(currentCellNameLabel.Text);

                if (contents is Formula)
                    input.Append("=" + contents.ToString());
                else
                    input.Append(contents.ToString());

                tb.AppendText(input.ToString());
            }
        }

        private void SSPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                SSPanel.GetSelection(out int col, out int row);
                InsertContents(col, row, false);
            }
            else if (e.KeyChar == (char)Keys.Up || e.KeyChar == (char)Keys.Down || e.KeyChar == (char)Keys.Left || e.KeyChar == (char)Keys.Right)
            {
                SSPanel.GetSelection(out int col, out int row);
                switch ((Keys)e.KeyChar)
                {
                    case Keys.Up:
                        SSPanel.SetSelection(col, row - 1);
                        break;
                    case Keys.Down:
                        SSPanel.SetSelection(col, row + 1);
                        break;
                    case Keys.Left:
                        SSPanel.SetSelection(col - 1, row);
                        break;
                    case Keys.Right:
                        SSPanel.SetSelection(col + 1, row);
                        break;
                }
                UpdateSelection(SSPanel);
            }
            else
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    if (input.Length > 0)
                    {
                        input.Remove(input.Length - 1, 1);
                    }

                    SSPanel.GetSelection(out int c, out int r);
                    string cell = LocationToCell(c, r);

                    DeleteSelectedCell();
                    Networking.SendSocket(serverSocket.theSocket, "edit " + cell + ":" + input.ToString() + ETX);
                }
                else if (!char.IsControl(e.KeyChar))
                {
                    input.Append(e.KeyChar);
                }

                SSPanel.GetSelection(out int col, out int row);
                lock(SSPanel)
                {
                    SSPanel.SetValue(col, row, input.ToString());
                }
            }

            e.Handled = true;
        }
        #endregion

        #endregion

        #region Ping

        /// <summary>
        /// Method to keep track of every 10 seconds
        /// </summary>
        private void InitTimer()
        {
            pingTimer = new System.Windows.Forms.Timer();
            pingTimer.Tick += new EventHandler(PingLoop);
            pingTimer.Interval = 10000;
            pingTimer.Start();
        }

        /// <summary>
        /// Called every 10 seconds to check to see if the client is still connected to the server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PingLoop(object sender, EventArgs e)
        {
            pingMiss++;
            //serverSocket.ClientCallback = ConfirmPing;

            string pingMessage = "ping " + ETX;

            try
            {

                if (serverSocket != null)
                    Networking.SendSocket(serverSocket.theSocket, pingMessage);
                //messages.Enqueue(pingMessage);
            }
            catch (Exception ex)
            {
                serverSocket = null;
                Invoke(new Action(() => SSPanel.Enabled = false));
                Invoke(new Action(() => editToolStripMenuItem.Enabled = false));
                Invoke(new Action(() => connectToolStripMenuItem.Enabled = true));
            }

            if (pingMiss > 6)
            {
                Disconnect();
                pingMiss = 0;
            }
        }

        /// <summary>
        /// Response when client recieves ping from server
        /// </summary>
        /// <param name="ss"></param>
        private void PingResponse(SocketState ss)
        {
            string pingResponse = "ping_response " + ETX;
            Networking.SendSocket(ss.theSocket, pingResponse);
            //messages.Enqueue(pingResponse);
        }

        /// <summary>
        /// Checks the message from server to verify ping
        /// </summary>
        /// <param name="ss"></param>
        //private void ConfirmPing(SocketState ss)
        //{
        //    String[] message = serverSocket.sb.ToString().Split(new string[] { " " }, StringSplitOptions.None);
        //    if (message[0] == "ping_response")
        //    {
        //        pingMiss = 0;
        //    }
        //}

        #endregion

        #region Disconnect

        /// <summary>
        /// Disconnects client from server
        /// </summary>
        public void Disconnect()
        {
            if (serverSocket != null)
            {
                string disconnectMessage = "disconnect " + ETX;
                Networking.SendSocket(serverSocket.theSocket, "unfocus " + ETX);
                Networking.SendSocket(serverSocket.theSocket, disconnectMessage);
                OnActiveSpreadsheet = false;
                Invoke(new Action(() => editToolStripMenuItem.Enabled = false));
                Invoke(new Action(() => connectToolStripMenuItem.Enabled = true));
                SSPanel.Invoke(new Action(SSPanel.Clear));
                SSPanel.Invoke(new Action(SSPanel.ClearActiveUsers));

                serverSocket = null;
            }
            SSPanel.Enabled = false;
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string undoMessage = "undo " + ETX;
            Networking.SendSocket(serverSocket.theSocket, undoMessage);
        }

        private void revertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string revertMessage = "revert " + currentCellNameLabel.Text + ETX;
            Networking.SendSocket(serverSocket.theSocket, revertMessage);
        }

        private void disconnectTab_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        /// <summary>
        /// Calls disconnect when the user closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpreadsheetForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Disconnect();
        }

        #endregion
    }
}
