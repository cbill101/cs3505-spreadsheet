﻿namespace SpreadsheetGUI
{
    partial class LoadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoadButton = new System.Windows.Forms.Button();
            this.spreadsheetText = new System.Windows.Forms.TextBox();
            this.info1 = new System.Windows.Forms.Label();
            this.spreadsheetList = new System.Windows.Forms.ListBox();
            this.info2 = new System.Windows.Forms.Label();
            this.info3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(240, 90);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 0;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // spreadsheetText
            // 
            this.spreadsheetText.Location = new System.Drawing.Point(207, 64);
            this.spreadsheetText.Name = "spreadsheetText";
            this.spreadsheetText.Size = new System.Drawing.Size(147, 20);
            this.spreadsheetText.TabIndex = 1;
            // 
            // info1
            // 
            this.info1.AutoSize = true;
            this.info1.Location = new System.Drawing.Point(178, 15);
            this.info1.Name = "info1";
            this.info1.Size = new System.Drawing.Size(195, 13);
            this.info1.TabIndex = 2;
            this.info1.Text = "Please enter the name of a spreadsheet";
            // 
            // spreadsheetList
            // 
            this.spreadsheetList.FormattingEnabled = true;
            this.spreadsheetList.Location = new System.Drawing.Point(9, 10);
            this.spreadsheetList.Margin = new System.Windows.Forms.Padding(2);
            this.spreadsheetList.Name = "spreadsheetList";
            this.spreadsheetList.Size = new System.Drawing.Size(165, 394);
            this.spreadsheetList.TabIndex = 3;
            // 
            // info2
            // 
            this.info2.AutoSize = true;
            this.info2.Location = new System.Drawing.Point(198, 29);
            this.info2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.info2.Name = "info2";
            this.info2.Size = new System.Drawing.Size(156, 13);
            this.info2.TabIndex = 4;
            this.info2.Text = "(If the spreasheet is not listed, a";
            // 
            // info3
            // 
            this.info3.AutoSize = true;
            this.info3.Location = new System.Drawing.Point(198, 43);
            this.info3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.info3.Name = "info3";
            this.info3.Size = new System.Drawing.Size(165, 13);
            this.info3.TabIndex = 5;
            this.info3.Text = "new spreadsheet will be created.)";
            // 
            // LoadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 414);
            this.Controls.Add(this.info3);
            this.Controls.Add(this.info2);
            this.Controls.Add(this.spreadsheetList);
            this.Controls.Add(this.info1);
            this.Controls.Add(this.spreadsheetText);
            this.Controls.Add(this.LoadButton);
            this.Name = "LoadForm";
            this.Text = "Load Spreadsheet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.TextBox spreadsheetText;
        private System.Windows.Forms.Label info1;
        private System.Windows.Forms.ListBox spreadsheetList;
        private System.Windows.Forms.Label info2;
        private System.Windows.Forms.Label info3;
    }
}