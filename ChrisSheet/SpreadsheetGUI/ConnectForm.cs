﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Controller;
using static Controller.NetworkController;

namespace SpreadsheetGUI
{
    /// <summary>
    /// Form to connect client to server
    /// </summary>
    public partial class ConnectForm : Form
    {
        //Checks connection status
        bool startupRecieved = false;

        //Holds main sheet
        SpreadsheetForm mainSheet;

        /// <summary>
        /// Creates connect form
        /// </summary>
        public ConnectForm(SpreadsheetForm sheet)
        {
            InitializeComponent();
            mainSheet = sheet;
        }

        /// <summary>
        /// Starts connection from client to server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connectbutton_Click(object sender, EventArgs e)
        {
            //Make sure text box isn't empty
            if (serverIpBox.Text == "")
            {
                MessageBox.Show("Please enter a server address");
                return;
            }

            //Disable controls
            serverIpBox.Enabled = false;
            connectButton.Enabled = false;

            try
            {
                Networking.ConnectToServer(FirstContact, serverIpBox.Text);
                Invoke(new Action(Close));
            }
            catch(ArgumentException)
            {
                MessageBox.Show("Invalid server name, please close the Connect Window and try again");
            }
        }

        /// <summary>
        /// Sends the initial register message to the server.
        /// </summary>
        /// <param name="state">contains network information</param>
        public void FirstContact(SocketState state)
        {
            state.ClientCallback = ReceieveStartup;

            string handshake = "register " + (char)3;
            Networking.SendSocket(state.theSocket, handshake);

            try
            {
                Networking.GetData(state);
            }
            catch (ObjectDisposedException)
            {
                MessageBox.Show("Server not found, please close the Connect Window and try again");
            }

        }

        /// <summary>
        /// Acknowledges that the Server has connected and recieves the initial list of
        /// spreasheets from the Server
        /// </summary>
        /// <param name="state">contains network information</param>
        private void ReceieveStartup(SocketState state)
        {
            mainSheet.CurrentServerHostname = serverIpBox.Text;

            String[] message = state.sb.ToString().Split(new string[] { " " }, StringSplitOptions.None);
            LoadForm loadForm = new LoadForm(mainSheet);
            //Checks to see if the first message is correct
            if (message[0] == "connect_accepted" && !startupRecieved)
            {
                startupRecieved = true;
                loadForm.SetServerSocket(state);
                string ETX = "" + (char)3;
                //Sees if there are no spreadsheets being sent
                if (message[1] == ETX)
                {
                    loadForm.ShowDialog();
                }
                else
                {
                    //Parses all spreadsheets
                    string sheetList = state.sb.ToString();
                    sheetList = sheetList.Substring(17);
                    sheetList = sheetList.Remove(sheetList.Length - 1);

                    String[] spreadsheets = sheetList.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                    //Starts listing each spreadsheet
                    foreach (string s in spreadsheets)
                    {
                        loadForm.AddSpreadsheet(s);
                    }

                    loadForm.ShowDialog();
                }
            }
            //An invalid message was sent
            else if(startupRecieved == false)
            {
                MessageBox.Show("Error connecting to server, try again");
            }
        }
    }
}
