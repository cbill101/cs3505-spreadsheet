﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using static Controller.NetworkController;

namespace SpreadsheetGUI
{
    /// <summary>
    /// Form to open spreadsheet
    /// </summary>
    public partial class LoadForm : Form
    {
        // Contains information for server
        private SocketState serverSocket;

        // Contains main spreadsheet
        private SpreadsheetForm mainSheet;

        /// <summary>
        /// Creates Load Form
        /// </summary>
        public LoadForm(SpreadsheetForm sheet)
        {
            InitializeComponent();
            mainSheet = sheet;
        }

        /// <summary>
        /// Adds spreadsheet to the list box
        /// </summary>
        /// <param name="spreadsheetName">name of the spreadsheet</param>
        public void AddSpreadsheet(string spreadsheetName)
        {
            spreadsheetList.Items.Add(spreadsheetName);
        }

        /// <summary>
        /// Sets server's socket for load form
        /// </summary>
        /// <param name="state"></param>
        public void SetServerSocket(SocketState state)
        {
            serverSocket = state;
        }

        /// <summary>
        /// Sends request to server to open specified spreadsheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            //Make sure text box isn't empty
            if (spreadsheetText.Text == "")
            {
                MessageBox.Show("Please enter a spreadsheet");
                return;
            }

            string loadMessage = "load " + spreadsheetText.Text + (char)3;
            if (serverSocket != null)
            {
                if(mainSheet.OnActiveSpreadsheet)
                {
                    Networking.SendSocket(serverSocket.theSocket, "unfocus " + (char)3);
                }
                //Prepares spreadsheet to fill with new information
                mainSheet.SetServerSocket(serverSocket);
                serverSocket.ClientCallback = mainSheet.FillNewSpreadsheet;
                Networking.SendSocket(serverSocket.theSocket, loadMessage);
                serverSocket.sb.Clear();
                Networking.GetData(serverSocket);
                Close();
            }
            else
            {
                MessageBox.Show("Please connect to a server first");
            }
        }

        //private void spreadsheetList_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    spreadsheetText.Text = spreadsheetList.SelectedItem.ToString();
        //}
    }
}
