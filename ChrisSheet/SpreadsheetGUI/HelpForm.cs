﻿// ===============================
// AUTHOR     : Chris Billingsley
// UID        : u1041095
// CLASS      : CS 3500
// PURPOSE    : To display help and tips on how the GUI and program overall works.
// NOTES      : None.
// ===============================
// Change History:
// See Git commits for details.
// ==================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpreadsheetGUI
{
    /// <summary>
    /// Represents a form that contains a tab control (to manage pages) and a close button to close the form.
    /// </summary>
    public partial class HelpForm : Form
    {
        /// <summary>
        /// Creates a new instance of a HelpForm.
        /// </summary>
        public HelpForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// When the Close button is clicked, the form closes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
