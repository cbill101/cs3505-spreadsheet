﻿PS6 - Thomas Oh, Mark Van der Merwe:

Design Thoughts:
10/21/2017 - Make top bar containing name of cell, contents in editable box, and value in non-editable box, all above the spreadsheet. Spreadsheet
and the top bar are designed (using TableLayoutPanel) to grow to fill the window as it resizes, and a minimum size is set.
10/22/2017 - Add the Spreadsheet object to the Spreadsheet Form object. On click, update top bar with selected cell's values.
10/23/2017 - Show descriptive error messages whenever an exception happens. We let the SaveFileDialog handle overwrite dangers (i.e., notify the user).
10/27/2017 - Added shortcuts and undo/redo function - undo/redo was done by using a list of previously changed cell names that are used to access
a dictionary mapping cell names to a list of their previous values. This allow you to walk back and forth through the changes.
Added export to excel (using microsoft's built in excel libraries) and csv (which simply writes comma-separated strings to a file).

Versions:
PS2: Version 1.1
Changed to pass all tests
PS3: Version 1.1
Changed to pass all tests
PS4: Version 1.1
Changed to pass all tests