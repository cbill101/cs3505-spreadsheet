﻿namespace SpreadsheetGUI
{
    partial class SpreadsheetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadsheetForm));
            this.spreadsheetMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenuTab = new System.Windows.Forms.ToolStripMenuItem();
            this.newSheetTab = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTab = new System.Windows.Forms.ToolStripMenuItem();
            this.openTab = new System.Windows.Forms.ToolStripMenuItem();
            this.clearTab = new System.Windows.Forms.ToolStripMenuItem();
            this.closeTab = new System.Windows.Forms.ToolStripMenuItem();
            this.helpTab = new System.Windows.Forms.ToolStripMenuItem();
            this.cellNameLabel = new System.Windows.Forms.Label();
            this.cellValueLabel = new System.Windows.Forms.Label();
            this.cellNameTextBox = new System.Windows.Forms.TextBox();
            this.cellValueTextBox = new System.Windows.Forms.TextBox();
            this.cellInformationPanel = new System.Windows.Forms.Panel();
            this.currentContentsTextBox = new System.Windows.Forms.TextBox();
            this.currentContentsLabel = new System.Windows.Forms.Label();
            this.spreadsheetSaveDialog = new System.Windows.Forms.SaveFileDialog();
            this.spreadsheetOpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.SSPanel = new SS.SpreadsheetPanel();
            this.spreadsheetMenuStrip.SuspendLayout();
            this.cellInformationPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // spreadsheetMenuStrip
            // 
            this.spreadsheetMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.spreadsheetMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuTab,
            this.helpTab});
            this.spreadsheetMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.spreadsheetMenuStrip.Name = "spreadsheetMenuStrip";
            this.spreadsheetMenuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.spreadsheetMenuStrip.Size = new System.Drawing.Size(1309, 28);
            this.spreadsheetMenuStrip.TabIndex = 0;
            this.spreadsheetMenuStrip.Text = "menuStrip1";
            // 
            // fileMenuTab
            // 
            this.fileMenuTab.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSheetTab,
            this.saveTab,
            this.openTab,
            this.clearTab,
            this.closeTab});
            this.fileMenuTab.Name = "fileMenuTab";
            this.fileMenuTab.Size = new System.Drawing.Size(44, 24);
            this.fileMenuTab.Text = "File";
            // 
            // newSheetTab
            // 
            this.newSheetTab.Name = "newSheetTab";
            this.newSheetTab.Size = new System.Drawing.Size(161, 26);
            this.newSheetTab.Text = "New";
            this.newSheetTab.Click += new System.EventHandler(this.NewTabClick);
            // 
            // saveTab
            // 
            this.saveTab.BackColor = System.Drawing.SystemColors.Control;
            this.saveTab.ForeColor = System.Drawing.SystemColors.ControlText;
            this.saveTab.Name = "saveTab";
            this.saveTab.Size = new System.Drawing.Size(161, 26);
            this.saveTab.Text = "Save";
            this.saveTab.Click += new System.EventHandler(this.SaveTabClick);
            // 
            // openTab
            // 
            this.openTab.Name = "openTab";
            this.openTab.Size = new System.Drawing.Size(161, 26);
            this.openTab.Text = "Open";
            this.openTab.Click += new System.EventHandler(this.OpenTabClick);
            // 
            // clearTab
            // 
            this.clearTab.Name = "clearTab";
            this.clearTab.Size = new System.Drawing.Size(161, 26);
            this.clearTab.Text = "Clear";
            this.clearTab.Click += new System.EventHandler(this.ClearTabClick);
            // 
            // closeTab
            // 
            this.closeTab.Name = "closeTab";
            this.closeTab.Size = new System.Drawing.Size(161, 26);
            this.closeTab.Text = "Close Sheet";
            this.closeTab.Click += new System.EventHandler(this.CloseTabClick);
            // 
            // helpTab
            // 
            this.helpTab.Name = "helpTab";
            this.helpTab.Size = new System.Drawing.Size(53, 24);
            this.helpTab.Text = "Help";
            this.helpTab.Click += new System.EventHandler(this.HelpTabClick);
            // 
            // cellNameLabel
            // 
            this.cellNameLabel.AutoSize = true;
            this.cellNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellNameLabel.Location = new System.Drawing.Point(4, 14);
            this.cellNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cellNameLabel.Name = "cellNameLabel";
            this.cellNameLabel.Size = new System.Drawing.Size(54, 17);
            this.cellNameLabel.TabIndex = 2;
            this.cellNameLabel.Text = "Name:";
            // 
            // cellValueLabel
            // 
            this.cellValueLabel.AutoSize = true;
            this.cellValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellValueLabel.Location = new System.Drawing.Point(121, 14);
            this.cellValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cellValueLabel.Name = "cellValueLabel";
            this.cellValueLabel.Size = new System.Drawing.Size(54, 17);
            this.cellValueLabel.TabIndex = 4;
            this.cellValueLabel.Text = "Value:";
            // 
            // cellNameTextBox
            // 
            this.cellNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.cellNameTextBox.Location = new System.Drawing.Point(69, 11);
            this.cellNameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cellNameTextBox.Name = "cellNameTextBox";
            this.cellNameTextBox.ReadOnly = true;
            this.cellNameTextBox.Size = new System.Drawing.Size(43, 22);
            this.cellNameTextBox.TabIndex = 7;
            this.cellNameTextBox.TabStop = false;
            // 
            // cellValueTextBox
            // 
            this.cellValueTextBox.Location = new System.Drawing.Point(179, 10);
            this.cellValueTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cellValueTextBox.Name = "cellValueTextBox";
            this.cellValueTextBox.ReadOnly = true;
            this.cellValueTextBox.Size = new System.Drawing.Size(101, 22);
            this.cellValueTextBox.TabIndex = 8;
            this.cellValueTextBox.TabStop = false;
            // 
            // cellInformationPanel
            // 
            this.cellInformationPanel.Controls.Add(this.currentContentsTextBox);
            this.cellInformationPanel.Controls.Add(this.currentContentsLabel);
            this.cellInformationPanel.Controls.Add(this.cellNameLabel);
            this.cellInformationPanel.Controls.Add(this.cellValueTextBox);
            this.cellInformationPanel.Controls.Add(this.cellNameTextBox);
            this.cellInformationPanel.Controls.Add(this.cellValueLabel);
            this.cellInformationPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.cellInformationPanel.Location = new System.Drawing.Point(0, 28);
            this.cellInformationPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cellInformationPanel.Name = "cellInformationPanel";
            this.cellInformationPanel.Size = new System.Drawing.Size(1309, 43);
            this.cellInformationPanel.TabIndex = 9;
            // 
            // currentContentsTextBox
            // 
            this.currentContentsTextBox.Location = new System.Drawing.Point(379, 11);
            this.currentContentsTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.currentContentsTextBox.Name = "currentContentsTextBox";
            this.currentContentsTextBox.Size = new System.Drawing.Size(136, 22);
            this.currentContentsTextBox.TabIndex = 11;
            this.currentContentsTextBox.TabStop = false;
            this.currentContentsTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CellContentsEnterPressed);
            // 
            // currentContentsLabel
            // 
            this.currentContentsLabel.AutoSize = true;
            this.currentContentsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentContentsLabel.Location = new System.Drawing.Point(289, 14);
            this.currentContentsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentContentsLabel.Name = "currentContentsLabel";
            this.currentContentsLabel.Size = new System.Drawing.Size(77, 17);
            this.currentContentsLabel.TabIndex = 10;
            this.currentContentsLabel.Text = "Contents:";
            // 
            // spreadsheetSaveDialog
            // 
            this.spreadsheetSaveDialog.DefaultExt = "sprd";
            this.spreadsheetSaveDialog.Filter = "Spreadsheet Files|.sprd|All Files|*.*";
            // 
            // spreadsheetOpenDialog
            // 
            this.spreadsheetOpenDialog.Filter = "Spreadsheet Files|*.sprd|All Files|*.*";
            // 
            // SSPanel
            // 
            this.SSPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SSPanel.Location = new System.Drawing.Point(0, 71);
            this.SSPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SSPanel.Name = "SSPanel";
            this.SSPanel.Size = new System.Drawing.Size(1309, 668);
            this.SSPanel.TabIndex = 1;
            this.SSPanel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SSPanel_KeyDown);
            // 
            // SpreadsheetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(1309, 740);
            this.Controls.Add(this.SSPanel);
            this.Controls.Add(this.cellInformationPanel);
            this.Controls.Add(this.spreadsheetMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.spreadsheetMenuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SpreadsheetForm";
            this.Text = "Spreadsheet";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpreadsheetFormClosingCheck);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SpreadsheetForm_KeyDown);
            this.spreadsheetMenuStrip.ResumeLayout(false);
            this.spreadsheetMenuStrip.PerformLayout();
            this.cellInformationPanel.ResumeLayout(false);
            this.cellInformationPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip spreadsheetMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenuTab;
        private System.Windows.Forms.ToolStripMenuItem newSheetTab;
        private System.Windows.Forms.ToolStripMenuItem saveTab;
        private System.Windows.Forms.ToolStripMenuItem openTab;
        private System.Windows.Forms.ToolStripMenuItem closeTab;
        private System.Windows.Forms.ToolStripMenuItem helpTab;
        private System.Windows.Forms.Label cellNameLabel;
        private System.Windows.Forms.Label cellValueLabel;
        private System.Windows.Forms.TextBox cellNameTextBox;
        private System.Windows.Forms.TextBox cellValueTextBox;
        private System.Windows.Forms.Panel cellInformationPanel;
        private System.Windows.Forms.TextBox currentContentsTextBox;
        private System.Windows.Forms.Label currentContentsLabel;
        private System.Windows.Forms.SaveFileDialog spreadsheetSaveDialog;
        private System.Windows.Forms.OpenFileDialog spreadsheetOpenDialog;
        private System.Windows.Forms.ToolStripMenuItem clearTab;
        private SS.SpreadsheetPanel SSPanel;
    }
}

