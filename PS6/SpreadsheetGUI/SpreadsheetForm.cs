﻿// ===============================
// AUTHOR     : Chris Billingsley
// UID        : u1041095
// CLASS      : CS 3500
// PURPOSE    : To serve as the GUI, house events, and manage overall flow of the program.
// NOTES      : None.
// ===============================
// Change History:
// See Git commits for details.
// ==================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SS;
using System.IO;

namespace SpreadsheetGUI
{
    /// <summary>
    /// Represents a SpreadsheetForm, with functions to navigate the GUI and modify a Spreadsheet object.
    /// </summary>
    public partial class SpreadsheetForm : Form
    {
        // The backbone of the entire program... the spreadsheet itself.
        private Spreadsheet spreadsheet;

        // The default cell that the spreadsheet program starts at.
        private const string DEFAULT_CELL = "A1";

        // Overwrite check; checks if writing over the file that was last saved to. If so, does not need to show an overwrite warning.
        private string mostRecentFileName;

        // modifiedCellStack keeps track of the modified cells for undoing.
        // cellContentsStack keeps track of the cell's old contents for undoing.
        private Stack<string> modifiedCellStack;
        private Stack<object> cellContentsStack;

        // Represents the copied contents when Ctrl + C is pressed.
        private object copiedContents;

        /// <summary>
        /// Creates a new SpreadsheetForm, with a menu, cell information panel, and SpreadsheetPanel.
        /// </summary>
        public SpreadsheetForm()
        {
            InitializeComponent();

            spreadsheet = new Spreadsheet(s => true, s => s, "PS6");

            modifiedCellStack = new Stack<string>();
            cellContentsStack = new Stack<object>();

            mostRecentFileName = "";

            copiedContents = null;

            SSPanel.SelectionChanged += UpdateSelection;

            SSPanel.KeybindDown += SSPanel_KeyDown;

            // Focus goes to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];

            // Convert from char int values to Spreadsheet coordinates.
            SSPanel.SetSelection(DEFAULT_CELL[0] - 65, int.Parse("" + DEFAULT_CELL[1]) - 1);
            UpdateSelection(SSPanel);
        }

        /// <summary>
        /// Creates a new SpreadsheetForm, with a menu, cell information panel, and SpreadsheetPanel.
        /// </summary>
        public SpreadsheetForm(string filename)
        {
            InitializeComponent();

            spreadsheet = new Spreadsheet(filename, s => true, s => s, "PS6");

            foreach (string cell in spreadsheet.GetNamesOfAllNonemptyCells())
            {
                UpdateTextValuesOnContentChangeOrLoad(cell);
            }

            modifiedCellStack = new Stack<string>();
            cellContentsStack = new Stack<object>();

            mostRecentFileName = "";

            copiedContents = null;

            SSPanel.SelectionChanged += UpdateSelection;

            SSPanel.KeybindDown += SSPanel_KeyDown;

            // Focus goes to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];

            // Convert from char int values to Spreadsheet coordinates.
            SSPanel.SetSelection(DEFAULT_CELL[0] - 65, int.Parse("" + DEFAULT_CELL[1]) - 1);
            UpdateSelection(SSPanel);
        }

        /// <summary>
        /// Serves as the selection handler for the program. Changes the cell text values on selection.
        /// </summary>
        /// <param name="sender"></param>
        private void UpdateSelection(SpreadsheetPanel sender)
        {
            SSPanel.GetSelection(out int col, out int row);
            SSPanel.GetValue(col, row, out string value);

            UpdateTextValuesOnSelection(col, row);
        }

        /// <summary>
        /// Helper methodf, called when the text values need to change after the selection changes.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        private void UpdateTextValuesOnSelection(int col, int row)
        {
            cellNameTextBox.Text = GetCellName();
            SSPanel.GetValue(col, row, out string value);
            cellValueTextBox.Text = value;
            currentContentsTextBox.Text = spreadsheet.GetCellContents(cellNameTextBox.Text).ToString();
        }

        /// <summary>
        /// Helper method, called when the text values need to change after contents of a cell are changed, or when a spreadsheet is opened.
        /// </summary>
        /// <param name="cell"></param>
        private void UpdateTextValuesOnContentChangeOrLoad(string cell)
        {
            int row, col;
            col = cell[0] - 65;
            row = int.Parse(cell.Substring(1)) - 1;
            SSPanel.SetValue(col, row, spreadsheet.GetCellValue(cell).ToString());
            SSPanel.GetValue(col, row, out string value);
            if(cell == GetCellName())
            {
                cellValueTextBox.Text = value;
            }
            currentContentsTextBox.Text = spreadsheet.GetCellContents(cellNameTextBox.Text).ToString();
        }

        /// <summary>
        /// Helper method to get the selected cell's name.
        /// </summary>
        /// <returns></returns>
        private string GetCellName()
        {
            SSPanel.GetSelection(out int col, out int row);
            return char.ConvertFromUtf32(col + 65) + (row + 1);
        }

        /// <summary>
        /// Helper method to copy the cell's contents when Ctrl + C is used.
        /// </summary>
        private void CopyCell()
        {
            object contents = spreadsheet.GetCellContents(GetCellName());
            copiedContents = contents;
        }

        /// <summary>
        /// Helper method to paste the cell's contents when Ctrl + V is used.
        /// </summary>
        private void PasteCell()
        {
            if (copiedContents == null)
                return;

            // This code block checks if the object is a string or double originally.
            // If not, treat as Formula.
            cellContentsStack.Push(spreadsheet.GetCellContents(GetCellName()));
            if(copiedContents is string || copiedContents is double)
            {
                spreadsheet.SetContentsOfCell(GetCellName(), copiedContents.ToString());
            }
            else
            {
                spreadsheet.SetContentsOfCell(GetCellName(), "=" + copiedContents.ToString());
            }

            UpdateTextValuesOnContentChangeOrLoad(GetCellName());
            modifiedCellStack.Push(GetCellName());
        }

        /// <summary>
        /// Deletes the cell. Also adds the contents and cell name to the history stacks.
        /// </summary>
        private void DeleteSelectedCell()
        {
            string cell = GetCellName();
            cellContentsStack.Push(spreadsheet.GetCellContents(cell));
            spreadsheet.SetContentsOfCell(cell, "");
            UpdateTextValuesOnContentChangeOrLoad(cell);

            modifiedCellStack.Push(cell);
        }

        /// <summary>
        /// Helper method to clear a spreadsheet without closing the program.
        /// </summary>
        private void ClearSpreadsheet()
        {
            // Create anew spreadsheet, then clear everything.
            spreadsheet = new Spreadsheet();
            SSPanel.Clear();
            currentContentsTextBox.Clear();
            cellValueTextBox.Clear();
            modifiedCellStack.Clear();
            cellContentsStack.Clear();
        }

        /// <summary>
        /// Helper method to open a Spreadsheet from a file.
        /// </summary>
        private void OpenSpreadsheet()
        {
            spreadsheet = new Spreadsheet(spreadsheetOpenDialog.FileName, s => true, s => s, "PS6");

            HashSet<string> nonEmptyCells = new HashSet<string>(spreadsheet.GetNamesOfAllNonemptyCells());

            SSPanel.Clear();

            foreach (string cell in nonEmptyCells)
            {
                UpdateTextValuesOnContentChangeOrLoad(cell);
            }
        }

        /// <summary>
        /// Saves the spreadsheet into the desired file. Also records the most recent file saved to, for overwrite tracking purposes.
        /// </summary>
        private void SaveSpreadsheet()
        {
            if (spreadsheetSaveDialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(spreadsheetSaveDialog.FileName) && mostRecentFileName != spreadsheetSaveDialog.FileName)
                {
                    DialogResult result = MessageBox.Show("You are overwriting an existing file. Are you sure you want to overwrite?", "Warning",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        spreadsheet.Save(spreadsheetSaveDialog.FileName);
                        mostRecentFileName = spreadsheetSaveDialog.FileName;
                    }
                }
                else
                {
                    spreadsheet.Save(spreadsheetSaveDialog.FileName);
                    mostRecentFileName = spreadsheetSaveDialog.FileName;
                }
            }
        }

        /// <summary>
        /// Helper method to insert cell contents, while updating history stacks and GUI information.
        /// </summary>
        private void InsertContents()
        {
            // Push cell name and contents onto the history stacks. 
            // Also, get the non empty cells and loop though and change information in the sheet and GUI.
            cellContentsStack.Push(spreadsheet.GetCellContents(cellNameTextBox.Text));
            HashSet<string> cells = new HashSet<string>(spreadsheet.SetContentsOfCell(cellNameTextBox.Text, currentContentsTextBox.Text));
            modifiedCellStack.Push(cellNameTextBox.Text);

            foreach (string cell in cells)
            {
                UpdateTextValuesOnContentChangeOrLoad(cell);
            }

            // Focus goes back to the spreadsheet itself.
            ActiveControl = SSPanel.Controls[0];
        }

        /// <summary>
        /// Helper method. When the program/sheet is closing, this will ask the user to save, showing a message box and acting
        /// on the user's choice accordingly.
        /// </summary>
        private void AskUserToSave(FormClosingEventArgs e)
        {
            // Shows a warning dialog that prmpts the user to save. 
            // Clicking "Yes" saves the sheet then closes the form, "No" closes the form, and "Cancel" cancels the event, leaving the form open.
            DialogResult result = MessageBox.Show("You have unsaved changes. Save now? Any unsaved changes will be lost!", "Warning",
            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                if (spreadsheetSaveDialog.ShowDialog() == DialogResult.OK)
                {
                    spreadsheet.Save(spreadsheetSaveDialog.FileName);
                }
            }
            else if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Shows the help dialog.
        /// </summary>
        private void ShowHelpDialog()
        {
            HelpForm helpForm = new HelpForm();
            helpForm.ShowDialog();
        }

        /// <summary>
        /// Undo the last operation.
        /// </summary>
        private void Undo()
        {
            // Check if stacks are empty or not. If not, undoing is possible.
            if(modifiedCellStack.Count > 0 && cellContentsStack.Count > 0)
            {
                // Retrieve the last cell name and contents and set the contents of that cell.
                string name = modifiedCellStack.Pop();
                object contents = cellContentsStack.Pop();

                if(contents is string || contents is double)
                {
                    spreadsheet.SetContentsOfCell(name, contents.ToString());
                }
                else
                {
                    spreadsheet.SetContentsOfCell(name, "=" + contents.ToString());
                }

                UpdateTextValuesOnContentChangeOrLoad(name);

                // Select the last cell that received content input.
                SSPanel.SetSelection(name[0] - 65, int.Parse("" + name.Substring(1)) - 1);
                UpdateSelection(SSPanel);
            }
        }

        /// <summary>
        /// When File -> New is clicked, runs a new Spreadsheet in a separate Form window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewTabClick(object sender, EventArgs e)
        {
            SpreadsheetApplicationContext.getAppContext().RunForm(new SpreadsheetForm());
        }

        /// <summary>
        /// When File -> Close is clicked, closes the spreadsheet form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseTabClick(object sender, EventArgs e)
        {
            Close();
        } 

        /// <summary>
        /// When File -> Save is clicked, the Spreadsheet is saved to a file of the user's choosing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveTabClick(object sender, EventArgs e)
        {
            SaveSpreadsheet();
        }

        /// <summary>
        /// Keeps track of how many top-level forms are running
        /// </summary>
        class SpreadsheetApplicationContext : ApplicationContext
        {
            // Number of open forms
            private int formCount = 0;

            // Singleton ApplicationContext
            private static SpreadsheetApplicationContext appContext;

            /// <summary>
            /// Private constructor for singleton pattern
            /// </summary>
            private SpreadsheetApplicationContext()
            {
            }

            /// <summary>
            /// Returns the one DemoApplicationContext.
            /// </summary>
            public static SpreadsheetApplicationContext getAppContext()
            {
                if (appContext == null)
                {
                    appContext = new SpreadsheetApplicationContext();
                }
                return appContext;
            }

            /// <summary>
            /// Runs the form
            /// </summary>
            public void RunForm(Form form)
            {
                // One more form is running
                formCount++;

                // When this form closes, we want to find out
                form.FormClosed += (o, e) => { if (--formCount <= 0) ExitThread(); };

                // Run the form
                form.Show();
            }

        }

        /// <summary>
        /// When File -> Open is clicked, opens a new Spreadsheet in the current wiodow.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenTabClick(object sender, EventArgs e)
        {
            if (spreadsheetOpenDialog.ShowDialog() == DialogResult.OK)
            {
                OpenSpreadsheet();
            }
        }

        /// <summary>
        /// When File -> Clear is clicked, it clears the Spreadsheet window and makes a new Spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearTabClick(object sender, EventArgs e)
        {
            ClearSpreadsheet();
        }

        /// <summary>
        /// When Help is clicked, the help window opens, presenting an overview of the program as well as the functions within.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpTabClick(object sender, EventArgs e)
        {
            ShowHelpDialog();
        }

        /// <summary>
        /// When the form is closing, this checks if the spreadsheet is modified. If so, prompt the user to save. Otherwise, close the program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpreadsheetFormClosingCheck(object sender, FormClosingEventArgs e)
        {
            // If the spreadsheet is modfied, show a warning.
            if (spreadsheet.Changed)
            {
                AskUserToSave(e);
            }
        }

        /// <summary>
        /// On KeyDown, process key bindings for the Spreadsheet, after navigation is processed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSPanel_KeyDown(object sender, KeyEventArgs e)
        {
            // Delete a cell
            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                DeleteSelectedCell();
                e.Handled = true;
            }
            // Cut the cell (delete cell while copying contents)
            else if(e.KeyData == (Keys.Control | Keys.X))
            {
                CopyCell();
                DeleteSelectedCell();
                e.Handled = true;
            }
            // Copy the cell's contents into the clipboard
            else if (e.KeyData == (Keys.Control | Keys.C))
            {
                CopyCell();
                e.Handled = true;
            }
            // Paste cell contents into the selected cell
            else if (e.KeyData == (Keys.Control | Keys.V))
            {
                PasteCell();
                e.Handled = true;
            }
            // Filler, direct other key presses to the form. If no hotkey registers, then focus goes to the text box.
            else
            {
                // Other keys wil report over to the form event, where general hotkeys reside.
                SpreadsheetForm_KeyDown(SSPanel, e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// General hotkeys are processed through here after keys are processed through the spreadsheet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpreadsheetForm_KeyDown(object sender, KeyEventArgs e)
        {
            // Pulls up the help dialog box.
            if (e.KeyData == (Keys.Control | Keys.H))
            {
                ShowHelpDialog();
            }
            // Makes a new speadsheet.
            else if(e.KeyData == (Keys.Control | Keys.N))
            {
                SpreadsheetApplicationContext.getAppContext().RunForm(new SpreadsheetForm());
            }
            // Saves the spreadsheet
            else if (e.KeyData == (Keys.Control | Keys.S))
            {
                SaveSpreadsheet();
            }
            // Opens a new spreadsheet.
            else if (e.KeyData == (Keys.Control | Keys.O))
            {
                if (spreadsheetOpenDialog.ShowDialog() == DialogResult.OK)
                {
                    OpenSpreadsheet();
                }
            }
            // Undo the last operation.
            else if(e.KeyData == (Keys.Control | Keys.Z))
            {
                Undo();
            }
            // Selects the default cell.
            else if(e.KeyData == (Keys.Control | Keys.Home))
            {
                SSPanel.SetSelection(DEFAULT_CELL[0] - 65, int.Parse("" + DEFAULT_CELL[1]) - 1);
                UpdateSelection(SSPanel);
            }
            // Clear spreadsheet
            else if(e.KeyData == (Keys.Control | Keys.T))
            {
                ClearSpreadsheet();
            }
            // Close the spreadsheet form.
            else if (e.KeyData == (Keys.Alt | Keys.F4))
            {
                Close();
            }
            // Enter the contents within.
            else if(e.KeyData == Keys.Enter)
            {
                ActiveControl = currentContentsTextBox;
            }
        }

        /// <summary>
        /// When the Enter key is pressed while the changeContentsTextBox has focus, the selected cell's contents and value change.
        /// The cell's information in the text boxes also change. The cell's old contents and its name are added to the history stacks
        /// as well.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellContentsEnterPressed(object sender, KeyEventArgs e)
        {
            // If inserting and calculating the contents throws an exception, an error message will be displayed.
            try
            {
                // Only detects enter, the rest of the keys don't matter for the text box.
                if (e.KeyCode == Keys.Enter)
                {
                    InsertContents();
                }
            }
            // Checks if an error is thrown when setting contents.
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
