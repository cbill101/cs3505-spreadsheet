﻿namespace SpreadsheetGUI
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.closeButton = new System.Windows.Forms.Button();
            this.helpTabControl = new System.Windows.Forms.TabControl();
            this.overviewTabPage = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.cellManipTabPage = new System.Windows.Forms.TabPage();
            this.cellManipRichTextBox = new System.Windows.Forms.RichTextBox();
            this.formulaTabPage = new System.Windows.Forms.TabPage();
            this.formulaHelpRichTextBox = new System.Windows.Forms.RichTextBox();
            this.hotkeyTabPage = new System.Windows.Forms.TabPage();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.helpTabControl.SuspendLayout();
            this.overviewTabPage.SuspendLayout();
            this.cellManipTabPage.SuspendLayout();
            this.formulaTabPage.SuspendLayout();
            this.hotkeyTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.closeButton.Location = new System.Drawing.Point(0, 875);
            this.closeButton.Margin = new System.Windows.Forms.Padding(4);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(1101, 47);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // helpTabControl
            // 
            this.helpTabControl.Controls.Add(this.overviewTabPage);
            this.helpTabControl.Controls.Add(this.cellManipTabPage);
            this.helpTabControl.Controls.Add(this.formulaTabPage);
            this.helpTabControl.Controls.Add(this.hotkeyTabPage);
            this.helpTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpTabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpTabControl.Location = new System.Drawing.Point(0, 0);
            this.helpTabControl.Margin = new System.Windows.Forms.Padding(4);
            this.helpTabControl.Name = "helpTabControl";
            this.helpTabControl.SelectedIndex = 0;
            this.helpTabControl.Size = new System.Drawing.Size(1101, 875);
            this.helpTabControl.TabIndex = 1;
            this.helpTabControl.Tag = "";
            // 
            // overviewTabPage
            // 
            this.overviewTabPage.Controls.Add(this.richTextBox1);
            this.overviewTabPage.Location = new System.Drawing.Point(4, 34);
            this.overviewTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.overviewTabPage.Name = "overviewTabPage";
            this.overviewTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.overviewTabPage.Size = new System.Drawing.Size(1093, 837);
            this.overviewTabPage.TabIndex = 0;
            this.overviewTabPage.Text = "Overview";
            this.overviewTabPage.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(1075, 306);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // cellManipTabPage
            // 
            this.cellManipTabPage.Controls.Add(this.cellManipRichTextBox);
            this.cellManipTabPage.Location = new System.Drawing.Point(4, 34);
            this.cellManipTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.cellManipTabPage.Name = "cellManipTabPage";
            this.cellManipTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.cellManipTabPage.Size = new System.Drawing.Size(1093, 837);
            this.cellManipTabPage.TabIndex = 1;
            this.cellManipTabPage.Text = "Cell Manipulation";
            this.cellManipTabPage.UseVisualStyleBackColor = true;
            // 
            // cellManipRichTextBox
            // 
            this.cellManipRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.cellManipRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cellManipRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.cellManipRichTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cellManipRichTextBox.Name = "cellManipRichTextBox";
            this.cellManipRichTextBox.ReadOnly = true;
            this.cellManipRichTextBox.Size = new System.Drawing.Size(1075, 289);
            this.cellManipRichTextBox.TabIndex = 1;
            this.cellManipRichTextBox.Text = resources.GetString("cellManipRichTextBox.Text");
            // 
            // formulaTabPage
            // 
            this.formulaTabPage.Controls.Add(this.formulaHelpRichTextBox);
            this.formulaTabPage.Location = new System.Drawing.Point(4, 34);
            this.formulaTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.formulaTabPage.Name = "formulaTabPage";
            this.formulaTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.formulaTabPage.Size = new System.Drawing.Size(1093, 837);
            this.formulaTabPage.TabIndex = 2;
            this.formulaTabPage.Text = "Formulas";
            this.formulaTabPage.UseVisualStyleBackColor = true;
            // 
            // formulaHelpRichTextBox
            // 
            this.formulaHelpRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.formulaHelpRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.formulaHelpRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formulaHelpRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.formulaHelpRichTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.formulaHelpRichTextBox.Name = "formulaHelpRichTextBox";
            this.formulaHelpRichTextBox.ReadOnly = true;
            this.formulaHelpRichTextBox.Size = new System.Drawing.Size(1093, 837);
            this.formulaHelpRichTextBox.TabIndex = 2;
            this.formulaHelpRichTextBox.Text = resources.GetString("formulaHelpRichTextBox.Text");
            // 
            // hotkeyTabPage
            // 
            this.hotkeyTabPage.Controls.Add(this.richTextBox2);
            this.hotkeyTabPage.Location = new System.Drawing.Point(4, 34);
            this.hotkeyTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.hotkeyTabPage.Name = "hotkeyTabPage";
            this.hotkeyTabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.hotkeyTabPage.Size = new System.Drawing.Size(1093, 837);
            this.hotkeyTabPage.TabIndex = 3;
            this.hotkeyTabPage.Text = "Hotkeys";
            this.hotkeyTabPage.UseVisualStyleBackColor = true;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.White;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(0, 0);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(1093, 837);
            this.richTextBox2.TabIndex = 0;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 922);
            this.Controls.Add(this.helpTabControl);
            this.Controls.Add(this.closeButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HelpForm";
            this.Text = "Spreadsheet Help";
            this.helpTabControl.ResumeLayout(false);
            this.overviewTabPage.ResumeLayout(false);
            this.cellManipTabPage.ResumeLayout(false);
            this.formulaTabPage.ResumeLayout(false);
            this.hotkeyTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.TabControl helpTabControl;
        private System.Windows.Forms.TabPage overviewTabPage;
        private System.Windows.Forms.TabPage cellManipTabPage;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox cellManipRichTextBox;
        private System.Windows.Forms.TabPage formulaTabPage;
        private System.Windows.Forms.RichTextBox formulaHelpRichTextBox;
        private System.Windows.Forms.TabPage hotkeyTabPage;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}