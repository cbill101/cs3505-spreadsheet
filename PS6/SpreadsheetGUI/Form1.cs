﻿using SpreadsheetUtilities;
using SS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace SpreadsheetGUI
{
    public partial class SpreadsheetForm : Form
    {

        /// <summary>
        /// The backend Spreadsheet logic object.
        /// </summary>
        private Spreadsheet spreadsheet;

        /// <summary>
        /// Filename represents where the current file should be saved.
        /// </summary>
        private string fileName;

        /// <summary>
        /// Keep track of the name of the selected cell.
        /// </summary>
        private string selectedCellName;

        /// <summary>
        /// List of the name of the cell that was last entered
        /// </summary>
        private LinkedList<string> undoCellList = new LinkedList<string>();

        /// <summary>
        /// Lift of cells contents according to cell name
        /// </summary>
        private Dictionary<string, LinkedList<string>> undoContentsList = new Dictionary<string, LinkedList<string>>();

        /// <summary>
        /// Number of undo elements held by the spreadsheet.
        /// </summary>
        private int undoSize = 50;

        /// <summary>
        /// Stack to store undo actions for redo
        /// </summary>
        private Stack<string> redoContentsStack = new Stack<string>();
        private Stack<string> redoCellStack = new Stack<string>();

        //**************Create new spreadsheet and handle basic setup***************//

        /// <summary>
        /// Constructor that takes in the name of a file to recreate a spreadsheet from.
        /// </summary>
        /// <param name="fileName">Name of saved spreadsheet file.</param>
        public SpreadsheetForm(string fileName)
        {
            try
            {
                spreadsheet = new Spreadsheet(fileName, s => true, s => s.ToUpper(), "ps6");
            }
            catch (SpreadsheetReadWriteException e)
            {
                ShowErrorMessage("Failed to Open", e.Message);
            }
            catch (Exception)
            {
                ShowErrorMessage("Failed to Open", "Something went wrong while opening the file! Please try again.");
            }
            
            this.fileName = fileName;
            SpreadsheetSetup();
        }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public SpreadsheetForm()
        {
            spreadsheet = new Spreadsheet(s => true, s => s.ToUpper(), "ps6");
            SpreadsheetSetup();
        }

        /// <summary>
        /// This constructor helper handles setting up our selectionchanged listener and some magic
        /// auto-gen windows forms stuff as well as setting up inital selections and values.
        /// </summary>
        private void SpreadsheetSetup()
        {
            InitializeComponent();

            // This an example of registering a method so that it is notified when
            // an event happens.  The SelectionChanged event is declared with a
            // delegate that specifies that all methods that register with it must
            // take a SpreadsheetPanel as its parameter and return nothing.  So we
            // register the displaySelection method below.

            // This could also be done graphically in the designer, as has been
            // demonstrated in class.
            spreadsheetPanel1.SelectionChanged += displaySelection;

            // Set a listener for when users hit enter in the contents textbox.
            contentsText.KeyUp += updateCellContents;
            
            //Sets an initial cell
            selectedCellName = "A1";

            // Show A1's information and focus on the textbox.
            UpdateContentsText();
            UpdateValueText();

            // Restore any values that are already set.
            UpdateCells(new HashSet<string>(spreadsheet.GetNamesOfAllNonemptyCells()));
        }

        /// <summary>
        /// When the form loads, immediately focus on context text so the user can immediately begin typing into A1 once the form loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpreadsheetForm_Shown(object sender, EventArgs e)
        {
            contentsText.Focus();
        }

        //*********Updating contents of Spreadsheet*********************//

        // Every time the selection changes, this method is called with the
        // Spreadsheet as its parameter.  Will allow the user to change the contents
        private void displaySelection(SpreadsheetPanel ss)
        {
            int row, col;
            // Selects the cell at the given row and column
            ss.GetSelection(out col, out row);

            // Sets the cells name, and puts it in the label.
            selectedCellName = GetCellName(col, row);
            cellLabel.Text = selectedCellName + ":";

            //Updates
            UpdateContentsText();
            UpdateValueText();
        }

        /// <summary>
        /// When the user presses enter in the contents text box, update the information in the backend,
        /// and use the returned set to update the values in the cells.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateCellContents(object sender, KeyEventArgs e)
        {
            // If user presses enter
            if (e.KeyCode == Keys.Enter)
            {
                addToUndoStack(contentsText.Text, selectedCellName);
                try
                {
                    spreadsheetUpdate(selectedCellName, contentsText.Text);
                }
                catch (CircularException)
                {
                    ShowErrorMessage("Circular Dependency", "A cell or chain of cells, depends on itself!");
                    UpdateContentsText();
                }
                catch (FormulaFormatException exception)
                {
                    ShowErrorMessage("Incorrect Formula", exception.Message);
                    UpdateContentsText();
                }
                catch (Exception)
                {
                    ShowErrorMessage("Unknown Error", "Something went wrong!");
                    UpdateContentsText();
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Updates spreadsheet according to inputed values
        /// </summary>
        /// <param name="cell">name of cell to update</param>
        /// <param name="contents">contents of cell to update</param>
        private void spreadsheetUpdate(string cell, string contents)
        {
            //Recieve list of cells to update, and go through and update them in the Spreadsheet Panel
            ISet<string> cellsToUpdate = spreadsheet.SetContentsOfCell(cell, contents);
            UpdateCells(cellsToUpdate);
            UpdateValueText();
            UpdateContentsText();
        }

        /// <summary>
        /// Updates the spreadsheet panel according to the cells to update
        /// </summary>
        /// <param name="cellsToUpdate">The set of cells to update</param>
        private void UpdateCells(ISet<string> cellsToUpdate)
        {
            foreach (string cell in cellsToUpdate)
            {
                GetCellLocation(cell, out int col, out int row);
                spreadsheetPanel1.SetValue(col, row, spreadsheet.GetCellValue(cell).ToString());
            }
        }

        /// <summary>
        /// Updates the value textbox of the current cell
        /// </summary>
        private void UpdateValueText()
        {
            valueText.Text = spreadsheet.GetCellValue(selectedCellName).ToString();
        }

        /// <summary>
        /// Updates the contents textbox of the current cell
        /// </summary>
        private void UpdateContentsText()
        {
            object cellContents = spreadsheet.GetCellContents(selectedCellName);
            if (cellContents is Formula)
            {
                contentsText.Text = "=" + cellContents.ToString();
            }
            else
            {
                contentsText.Text = cellContents.ToString();
            }

            // Focus the contents text we just updated.
            contentsText.Focus();
        }

        //***************Handle file creation, opening, and saving***************//

        /// <summary>
        /// Creates a new form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newMenu_Click(object sender, EventArgs e)
        {
            SpreadsheetApplicationContext context = SpreadsheetApplicationContext.getAppContext();
            SpreadsheetForm newSpreadsheet = new SpreadsheetForm();
            newSpreadsheet.Text = "Spreadsheet" + (SpreadsheetApplicationContext.getAppContext().FormCount + 1);

            // Tell the application context to run the form on the same
            // thread as the other forms.
            context.RunForm(newSpreadsheet);
        }

        /// <summary>
        /// Opens a file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openMenu_Click(object sender, EventArgs e)
        {
            // Use OpenFileDialog to abstract details of opening files.
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Spreadsheet file (.sprd)| *.sprd";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string newFile = openFileDialog.FileName;
                SpreadsheetApplicationContext.getAppContext().RunForm(new SpreadsheetForm(newFile));
            }
        }

        /// <summary>
        /// Save
        /// s over the current file, unless there is none, which then proceeds to call save as
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveMenu_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                SaveAsNewFile();
            }
            else
            {
                spreadsheet.Save(fileName);
            }
        }

        /// <summary>
        /// Saves a new file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAsNewFile();
        }

        /// <summary>
        /// Helps save a new file
        /// </summary>
        private void SaveAsNewFile()
        {
            // Abstract file save details with the SaveFileDialog.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Spreadsheet file (.sprd)| *.sprd";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;
                spreadsheet.Save(fileName);
            }
        }

        //******************Access Helper Methods/Error Helper Methods*****************//

        /// <summary>
        /// Given the column and row numbers, determine the corresponding cell name.
        /// </summary>
        /// <param name="row">Row the cell is in</param>
        /// <param name="col">Column the cell is in</param>
        /// <returns></returns>
        private string GetCellName(int col, int row)
        {
            char letter = (char)(col + 65);
            string name = letter + (row + 1).ToString();
            return name;
        }

        /// <summary>
        /// Returns the column and the row of the named cell
        /// </summary>
        /// <param name="cellName">Name of the cell</param>
        /// <param name="col">Column the cell is in</param>
        /// <param name="row">Row the cell is in</param>
        private void GetCellLocation(string cellName, out int col, out int row)
        {
            col = (int)cellName[0] - 65;
            if (int.TryParse(cellName.Substring(1), out int _row))
            {
                row = _row - 1;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Produces an error message
        /// </summary>
        /// <param name="title">The title of the error</param>
        /// <param name="message">The message to be shown</param>
        private void ShowErrorMessage(string title, string message)
        {
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //***************Handle dangerous saves********************//

        /// <summary>
        /// Warning if the user tries to close a spreadsheet that has changes that haven't been saved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (spreadsheet.Changed)
            {
                var choice = MessageBox.Show("Changes have been made that will be lost! " +
                    "Would you like to save before exiting?", "Save?", MessageBoxButtons.YesNoCancel);

                if (choice == DialogResult.Yes)
                {
                    saveMenu_Click(null, null);
                }
                else if (choice == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        //******************Help Message**********************//

        /// <summary>
        /// Help text that appears when the user clicks the help menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome to the most exciting testing session of your life, this program will allow" +
                " you to input numbers, words, and formulas into cells and store the information. \n" +
                "To get started, go ahead and select a cell and you will be able to start typing inside " +
                "the contents box on the top left above the spreadsheet. You will need to press \"Enter\" " +
                "in order to confirm the contents of the cell. In order to set a cell's contents to a formula, " +
                "you must put an equals sign in front of the equation. You can refer to other cells with lower or " +
                "upper case letters. \nYou can utilize the \"File\" menu to either make a new spreadsheet in a new " +
                "window, open a previously saved spreadsheet, save a current spreadsheet, or save the current " +
                "spreadsheet as a new file. \nShortcuts have been added to allow the user to be lazy. \"Ctrl + N\" " +
                "opens a new spreadsheet in a new window, \"Ctrl + S\" saves the spreadsheet, \"Ctrl + Shift + S\" " +
                "saves the spreadsheet as a new file, \"Ctrl + O\" opens a saved spreadsheet. \nSince our program" +
                " is insignificant compared Excel, the user can export the spreadsheet as an Excel or a .csv file. " +
                "\nThe spreadsheet also comes with undo and redo functionality in the \"Edit\" menu, however, it can" +
                " only undo up to 50 times so that it doesn't take up too much memory.\n" +
                "To export your code to an inferior spreadsheet program (i.e. Excel) or to a CSV (comma-separated-value)" +
                "you can simply click on export in the top menu and select excel or csv.",
                "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //******************Undo/Redo************************//

        /// <summary>
        /// Undoes the last action on the undo stack.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (undoCellList.Count != 0)
            {
                //Store action to redo
                redoContentsStack.Push(undoContentsList[undoCellList.Last()].Last());
                redoCellStack.Push(undoCellList.Last());

                //Either undoes last action by emptying what is there, or putting back what used to be there.
                if (undoContentsList[undoCellList.Last()].Count < 2)
                {
                    spreadsheetUpdate(undoCellList.Last(), "");
                }
                else
                {
                    spreadsheetUpdate(undoCellList.Last(), undoContentsList[undoCellList.Last()].ElementAt(undoContentsList[undoCellList.Last()].Count - 2));
                }

                //Removes from undo list
                undoContentsList[undoCellList.Last()].RemoveLast();
                undoCellList.RemoveLast();
            }
        }
        
        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(redoCellStack.Count != 0)
            {
                //Empties and does redo
                spreadsheetUpdate(redoCellStack.Peek(), redoContentsStack.Peek());

                //Refills undo
                addToUndoStack(redoContentsStack.Peek(), redoCellStack.Peek());

                //Removes from redo list
                redoCellStack.Pop();
                redoContentsStack.Pop();
            }
        }

        /// <summary>
        /// Adds contents of cell to a linked list. This WILL store blanks,
        /// essentially to erase a cell with contents.
        /// </summary>
        /// <param name="contents"></param>
        private void addToUndoStack(string contents, string cell)
        {
            if (!string.IsNullOrEmpty(contents))
            {
                //Checks to see if there are more than 50 items in the list
                if (undoCellList.Count > undoSize)
                {
                    undoContentsList[undoCellList.First()].RemoveFirst();
                    undoCellList.RemoveFirst();
                }
                undoCellList.AddLast(cell);
                //Checks to see if dictionary already contains value
                if(undoContentsList.ContainsKey(cell))
                {
                    undoContentsList[cell].AddLast(contents);
                }
                else
                {
                    LinkedList<string> cellContents = new LinkedList<string>();
                    cellContents.AddLast(contents);
                    undoContentsList.Add(cell, cellContents);
                }
            }
        }

        //************Export*****************//

        /// <summary>
        /// Export the data from our spreadsheet software into excel by opening a new excel file and 
        /// adding the cell information in.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application excel;
            Workbook workbook;
            Worksheet worksheet;
            try
            {
                // Open a new excel file.
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = true;
                workbook = excel.Workbooks.Add("");
                worksheet = workbook.ActiveSheet;

                // For every string, add to the corresponding cell in excel.
                foreach (string cell in spreadsheet.GetNamesOfAllNonemptyCells())
                {
                    object contents = spreadsheet.GetCellContents(cell);
                    GetCellLocation(cell, out int col, out int row);

                    if (contents is Formula)
                    {
                        worksheet.Cells[row + 1, col + 1] = "=" + ((Formula)contents).ToString();
                    }
                    else
                    {
                        worksheet.Cells[row + 1, col + 1] = contents;
                    }
                }
            } catch (Exception)
            {
                ShowErrorMessage("Failed to Export", "Failed to export the spreadsheet to excel. Please try again.");
            }
        }

        private void cSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StringBuilder csv = new StringBuilder();

            // For every string, write a comma-separated value.
            foreach (string cell in spreadsheet.GetNamesOfAllNonemptyCells())
            {
                object contents = spreadsheet.GetCellContents(cell);

                if (contents is Formula)
                {
                    string cont = "=" + ((Formula)contents).ToString();
                    string newCell = string.Format("{0},{1}", cell, cont);
                    csv.AppendLine(newCell);
                }
                else
                {
                    string newCell = string.Format("{0},{1}", cell, contents.ToString());
                    csv.AppendLine(newCell);
                }
            }

            // Write to a csv file named the same as the current file.
            // Abstract file save details with the SaveFileDialog.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Spreadsheet file (.csv)| *.csv";
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;
                File.WriteAllText(fileName, csv.ToString());
            }
        }
    }
}
